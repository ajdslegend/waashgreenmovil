import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Events} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Headers, Http} from '@angular/http';

import {UtilServiceProvider} from '../util-service/util-service';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AuthServiceProvider {
  public currentUser: any = null;
  public token: string;
  CURRENT_USER = 'currentUser';
  TOKEN_SESSION = 'tokenSession';
  private headers = new Headers({'Content-Type': 'application/json'});
  private api;

  constructor(
    public events: Events,
    public storage: Storage,
    public http: Http,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  public login(credentials): Promise<any> {
    return this.http.post(this.api + "/signin", credentials, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          this.currentUser = rsp.response.user;
          this.token = rsp.response.token;
          this.storage.set(this.CURRENT_USER, this.currentUser);
          this.storage.set(this.TOKEN_SESSION, this.token);
          this.events.publish('user:login', this.currentUser);
          return {success: true, role: this.currentUser.profile.role};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public register(credentials): Promise<any> {
    return this.http.post(this.api + "/signup", credentials, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          if (credentials.role != "provider") {
            this.currentUser = rsp.response.user;
            this.token = rsp.response.token;
            this.storage.set(this.CURRENT_USER, this.currentUser);
            this.storage.set(this.TOKEN_SESSION, this.token);
            this.events.publish('user:login', this.currentUser);
            return {success: true, role: this.currentUser.profile ? this.currentUser.profile.role : ""};
          }
          else {
            return {success: true};
          }

        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public getUserInfo(): Promise<any> {
    return this.storage.get(this.CURRENT_USER).then((value) => {
      return value;
    });
  };

  public getSessionToken(): Promise<any> {
    return this.storage.get(this.TOKEN_SESSION).then((value) => {
      return value;
    });
  };


  // getToken: Observable<any> =
  //   Observable.fromPromise(this.storage.get(this.TOKEN_SESSION).then(value => {
  //     return value;
  //   }));
  getToken: Observable<any> =
    Observable.create(this.storage.get(this.TOKEN_SESSION).then(value => {
      return value;
    }));

  public logout() {

    this.getSessionToken().then((token) => {
      var headers = new Headers();
      headers.append('x-access-token', token);
      var options = {headers: headers};
      return this.http.get(this.api + "/signout", options).toPromise();
    })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          console.log(rsp);
        }
        else {
          console.log("error signout", rsp);
        }
      })
      .catch(error => {
        console.log("error signout", error);
      });

    return Observable.create(observer => {
      this.currentUser = null;
      this.storage.set(this.CURRENT_USER, this.currentUser);
      observer.next(true);
      observer.complete();
    });
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }

  public facebookLogin(access_token): Promise<any> {
    return this.http.post(this.api + "/accounts/facebook", {token: access_token})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          this.currentUser = rsp.response.user;
          this.token = rsp.response.token;
          this.storage.set(this.CURRENT_USER, this.currentUser);
          this.storage.set(this.TOKEN_SESSION, this.token);
          this.events.publish('user:login', this.currentUser);
          return {success: true, role: this.currentUser.profile ? this.currentUser.profile.role : ""};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public forgotPassword(email): Promise<any> {
    return this.http.post(this.api + "/forgot-password", {email: email}, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public updateUser(user) {
    return Observable.create(observer => {
      this.currentUser = user;
      this.storage.set(this.CURRENT_USER, user);
      observer.next(true);
      observer.complete();
    });
  }


}
