import {Injectable} from '@angular/core';

import {Platform} from 'ionic-angular';

import {Observable} from 'rxjs/Observable';

import {Transfer, TransferObject} from "@ionic-native/transfer";
import {Http} from '@angular/http';
import {File} from '@ionic-native/file';

import 'rxjs/add/operator/toPromise';

import {AlertController} from 'ionic-angular';

declare var cordova: any;

@Injectable()
export class UtilServiceProvider {

  private api = 'https://waashgreenapi.herokuapp.com/api';
  // private api = 'https://waashgreenapitest.herokuapp.com/api';
  // TEST-435fe83e-19e1-4f44-a629-b2a538b99fad
  // APP_USR-30997425-998d-41f0-9ed9-91f66b04b5b8
  // public public_key='TEST-435fe83e-19e1-4f44-a629-b2a538b99fad';
  public public_key='APP_USR-30997425-998d-41f0-9ed9-91f66b04b5b8'
  // private api = 'http://192.168.10.254:3001/api';
  // private api = 'http://192.168.1.116:3001/api';
  public regions = null;
  public countries = null;

  constructor(
    public http: Http,
    private transfer: Transfer,
    private file: File,
    private alertCtrl: AlertController,
    public platform: Platform
  ) {
  }

  public getCountries(): Promise<any> {
    if (this.countries == null) {
      return this.http.get(this.api + "/countries")
        .toPromise()
        .then(response => {
          let rsp = response.json();
          if (rsp.success) {
            this.countries = rsp.response.countries;
            return {success: true, data: rsp.response.countries};
          }
          else {
            this.handleError(rsp);
          }
        })
        .catch(error => {
          return this.handleError(error.json());
        });
    }
    else {
      return new Promise((resolve, reject) => resolve({success: true, data: this.countries}));
    }
  }

  public getRegions(): Promise<any> {
    if (this.regions == null) {
      return this.http.get(this.api + "/regions")
        .toPromise()
        .then(response => {
          let rsp = response.json();
          if (rsp.success) {
            this.regions = rsp.response.regions;
            return {success: true, data: rsp.response.regions};
          }
          else {
            this.handleError(rsp);
          }
        })
        .catch(error => {
          return this.handleError(error.json());
        });
    }
    else {
      //debugger;
      return new Promise((resolve, reject) => resolve({success: true, data: this.regions}));
    }
  }

  public resetRegions() {
    this.regions = null;
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }


  public uploadFile(targetPath, options): Promise<any> {

    var url = this.api + "/upload";

    const fileTransfer: TransferObject = this.transfer.create();

    return fileTransfer.upload(targetPath, url, options).then(data => {
      let rsp = JSON.parse(data.response);
      if (rsp.success) {
        return {success: true, data: rsp.response.file_url};
      }
      else {
        this.handleError(rsp);
      }
    }, err => {
      return this.handleError(JSON.parse(err.body));
    });


  }

  public uploadFormFile(file, options): Promise<any> {
    var url = this.api + "/upload";

    let formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('name', options.params.name);
    formData.append('rut', options.params.rut);

    return this.http.post(url, formData)
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response.file_url};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public downloadFile(url, name): Promise<any> {
    const fileTransfer: TransferObject = this.transfer.create();
    var promise = fileTransfer.download(url, this.file.externalDataDirectory + url.split("/")[url.split("/").length - 1]).then((entry) => {
      console.log('download complete: ' + entry.toURL());

      const alertSuccess = this.alertCtrl.create({
        title: "Descarga Exitosa!",
        subTitle: "Archivo fue descargado en:" + entry.toURL(),
        buttons: ['Ok']
      });

      alertSuccess.present();

      return {success: true, url: entry.toURL()};

    }, err => {
      const alertFailure = this.alertCtrl.create({
        title: `Download Failed!`,
        subTitle: `File was not successfully downloaded. Error code: ${err}`,
        buttons: ['Ok']
      });

      alertFailure.present();
      return this.handleError(JSON.parse(err.body));
    });

    fileTransfer.onProgress((progressEvent) => {
      console.log(progressEvent);
      var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
      //this.progress = perc;
      console.log(perc);

    });

    return promise;


  }


  public getCountriesByWord(keyword: string): Promise<any> {
    let countries = [];
    return this.getCountries().then(rsp => {
        if (rsp.success) {
          countries = rsp.data;
        }
        return countries.filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()));
      },
      error => {
        return countries;
      })
      .catch((err) => {
        console.log(err);
        return countries;
      });

  }

  formatHour(hour, minutes): String {
    var isAm = hour < 12;
    hour = (hour > 12) ? hour % 12 : (hour == 0) ? 12 : hour;
    var resultHour = (hour.toString().length == 1) ? "0" + hour.toString() : hour.toString();
    var resultMinutes = (minutes.toString().length == 1) ? "0" + minutes.toString() : minutes.toString();
    return resultHour + ":" + resultMinutes + (isAm ? "am" : "pm");
  }

  getCarClass(carType): String {
    var carClass = "";
    switch (carType) {
      case "a" :
        carClass = "Turismo pequeño";
        break;
      case "b" :
        carClass = "Turismo Mediano";
        break;
      case "c" :
        carClass = "Turismo gama alto";
        break;
      case "d" :
        carClass = "Todo terreno";
        break;
    }
    return carClass;
  }

  public downloadFileUrl(url): Promise<any> {

    let storageDirectory: string = '';

    if (this.platform.is('ios')) {
      storageDirectory = cordova.file.documentsDirectory;
    }
    else if (this.platform.is('android')) {
      storageDirectory = cordova.file.dataDirectory;
    }

    const fileTransfer: TransferObject = this.transfer.create();

    return fileTransfer.download(url, this.file.externalDataDirectory + name).then((entry) => {
      // debugger;
      console.log('download complete: ' + entry.toURL());
      const alertSuccess = this.alertCtrl.create({
        title: "Descarga Exitosa!",
        subTitle: "fue descargado en: " + entry.toURL(),
        buttons: ['Aceptar']
      });

      alertSuccess.present();

      return {success: true, url: entry.toURL()};

    }, err => {
      const alertFailure = this.alertCtrl.create({
        title: `Download Failed!`,
        subTitle: `File was not successfully downloaded. Error code: ${err}`,
        buttons: ['Ok']
      });

      alertFailure.present();
      return this.handleError(JSON.parse(err.body));
    });
  }

  public getBaseUrl() {
    return Observable.create(observer => {
      observer.next(this.api);
      observer.complete();
    });
  }

  formatearNumero(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
  }

}
