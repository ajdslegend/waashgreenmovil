import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';
import {Headers, Http} from '@angular/http';
import {AuthServiceProvider} from '../auth-service/auth-service';

import {UtilServiceProvider} from '../util-service/util-service';

/*
  Generated class for the CouponProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CouponProvider {

  private headers;

  private api;


  constructor(
    public events: Events,
    public http: Http,
    private auth: AuthServiceProvider,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }

  public getCampaigns(): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.get(this.api + "/client/campaign", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.campaigns.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public getCoupons(): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.get(this.api + "/client/coupons", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.coupons.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public addCoupons(couponData): Promise<any> {

    return  this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.patch(this.api + "/client/add-coupon", couponData, options).toPromise()
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response.data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public getCouponsApplied(couponData): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.post(this.api + "/client/coupons/by-type-service", couponData, options)
          .toPromise()
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.coupons.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public getAdminCoupons(status = '', id = ''): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.get(this.api + "/admin/coupons/?status=" + status + "&&campaign_id=" + id, options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.coupons.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public getAdminCampaign(status = ''): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.get(this.api + "/admin/campaign/?status=" + status, options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.campaigns.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public addAdminCampaign(couponData): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.post(this.api + "/admin/campaign", couponData, options)
          .toPromise()
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public updateAdminCampaign(couponData, id): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.post(this.api + "/admin/campaign/" + id, couponData, options)
          .toPromise()
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

}
