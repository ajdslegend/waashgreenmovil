import {Injectable} from '@angular/core';

import {Events} from 'ionic-angular';
import {Headers, Http} from '@angular/http';
import {AuthServiceProvider} from '../auth-service/auth-service';

import {UtilServiceProvider} from '../util-service/util-service';


import 'rxjs/add/operator/map';

@Injectable()
export class ClientProvider {

  private headers;
  private api;

  constructor(
    public events: Events,
    public http: Http,
    private auth: AuthServiceProvider,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  public getcars(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        let options = status != 'all' ? {params: {status: status}, headers: this.headers} : {headers: this.headers};
        return this.http.get(this.api + "/client/cars", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.cars.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public registercars(boddy): Promise<any> {

    return this.http.post(this.api + "/client/cars", boddy, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }

  public getcarsBrands(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = status != 'all' ? {params: {status: status}, headers: this.headers} : {headers: this.headers};
        return this.http.get(this.api + "/client/car_brands", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.car_brands.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public deletCar(carId): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {car_id: carId}, headers: this.headers};
        return this.http.delete(this.api + "/client/cars", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {

          return {success: true, data: rsp};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public requestRate(rate, id, provider): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        //var options = {params: {rate:rate,request_id:id}, headers: this.headers } ;
        return this.http.patch(this.api + "/client/request/review", {
          review: rate,
          request_id: id,
          provider: provider
        }, {headers: this.headers}).toPromise();

      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.msg;
          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public CreateRequest(requestData): Promise<any> {

    return this.http.post(this.api + "/client/request", requestData, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public requestStatus(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        let options = status != 'all' ? {params: {status: status}, headers: this.headers} : {headers: this.headers};
        return this.http.patch(this.api + "/client/request/status", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public getclientRequest(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        const options = {headers: this.headers};
        return this.http.get(this.api + "/client/requests", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.requests.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public getproviderDetail(id, listType, listIdx): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        const options = {params: {id: id}, headers: this.headers};
        return this.http.get(this.api + "/client/provider-detail", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {

          return {success: true, data: rsp.response.provider, listType: listType, listIdx: listIdx};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public getClientCarDetail(id, listType, listIdx): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        let options = {params: {id: id}, headers: this.headers};
        return this.http.get(this.api + "/client/car/detail", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {

          return {success: true, data: rsp.response.car, listType: listType, listIdx: listIdx};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public changeProfile(profileData): Promise<any> {

    return this.http.patch(this.api + "/client/change-profile", profileData, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  checkRegion(commune: String): Promise<any> {

    return this.auth.getSessionToken()
      .then((token: any) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.get(this.api + "/regions/check?name=" + commune, {headers: this.headers}).toPromise()
          .then(response => {
            let rsp = response.json();
            if (rsp.success && rsp.response.valid_region) {
              return {success: true};
            }
            else {
              return {success: false}
            }
          })
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  getBudget(cardId: String, type: String): Promise<any> {

    return this.auth.getSessionToken()
      .then((token: any) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.get(this.api + "/client/request/budget?car_id=" + cardId + "&type=" + type, {headers: this.headers}).toPromise()
          .then(response => {
            let rsp = response.json();
            if (rsp.success && rsp.response.total) {
              return {success: true, total: rsp.response.total};
            }
            else {
              return {success: false}
            }
          })
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


}
