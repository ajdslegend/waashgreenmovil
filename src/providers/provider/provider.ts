import {Injectable} from '@angular/core';

import {Events} from 'ionic-angular';
import {Headers, Http} from '@angular/http';
import {AuthServiceProvider} from '../auth-service/auth-service';

import {UtilServiceProvider} from '../util-service/util-service';


@Injectable()
export class providerProvider {

  private headers;

  private api;


  constructor(
    public events: Events,
    public http: Http,
    private auth: AuthServiceProvider,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }


  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }

  public getRequestsPending(): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {headers: this.headers};
        return this.http.get(this.api + "/provider/requests", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.requests.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public getRequestsPendingId(id): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {id: id}, headers: this.headers}
        return this.http.get(this.api + "/provider/requests", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.requests.map(request => {
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public getRequestsRecord(status): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {status: status}, headers: this.headers}
        return this.http.get(this.api + "/provider/requests/record", options).toPromise();

      })
      .then(response => {
        let rsp = response.json();

        if (rsp.success) {
          return {success: true, data: rsp.response.requests};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


  public getCustomerDetail(id, listType, listIdx): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {id: id}, headers: this.headers};
        return this.http.get(this.api + "/provider/customer-detail", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {

          return {success: true, data: rsp.response.customer, listType: listType, listIdx: listIdx};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public takeRequest(Id): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.patch(this.api + "/provider/request/take", {request_id: Id}, {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


  public completeRequest(Id, url, observations): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.patch(this.api + "/provider/request/complete", {
          request_id: Id,
          observations: observations,
          photo_url: url
        }, {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public cancelRequest(Id, observations): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.patch(this.api + "/provider/request/cancel", {
          request_id: Id,
          observations: observations
        }, {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


}
