import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {Firebase} from '@ionic-native/firebase';
import {Platform} from 'ionic-angular';

import {UtilServiceProvider} from '../util-service/util-service';

import {AuthServiceProvider} from '../auth-service/auth-service';

import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class FcmProvider {

  private api;

  constructor(
    public firebaseNative: Firebase,
    private platform: Platform,
    public http: Http,
    public util: UtilServiceProvider,
    private auth: AuthServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  async getAndSaveToken() {
    let token;
    let platform = "";
    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken();
      platform = "android";
    }
    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
      platform = "ios";
    }

    this.firebaseNative.onTokenRefresh()
      .subscribe((token: string) => {
        this.saveToken({token: token, platform: platform})
      });

    return this.saveToken({token: token, platform: platform});
  }

  private saveToken(params) {
    // debugger;
    if (!params.token) return;
    this.auth.getSessionToken().then((token) => {
      let headers = new Headers();
      headers.append('x-access-token', token);
      return this.http.post(this.api + "/notification-token", params, {headers: headers}).toPromise()
    })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          console.log("get save token")
          console.log(rsp);
        }
        else {
          console.log("error push registration", rsp);
        }
      })
      .catch(error => {
        console.log("error push registration", error);
      });

  }

  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen();
  }


}
