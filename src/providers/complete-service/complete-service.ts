import {AutoCompleteService} from 'ionic2-auto-complete';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map'
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import {UtilServiceProvider} from '../util-service/util-service';


@Injectable()
export class CompleteService implements AutoCompleteService {
  labelAttribute = "name";

  constructor(
    private util: UtilServiceProvider
  ) {
  }

  getResults(keyword: string) {
    debugger;
    let results = new BehaviorSubject([]);

    setTimeout(async () => {
      let countries = await this.util.getCountriesByWord(keyword);
      results.next(countries.filter(country => {
        return country.name.toLowerCase().startsWith(keyword.toLowerCase())
      }));
    });

    return results;

  }
}
