import {Injectable} from '@angular/core';

import {Events, Platform} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Headers, Http} from '@angular/http';

import {AuthServiceProvider} from '../auth-service/auth-service';

import moment from 'moment';

import {AlertController} from 'ionic-angular';

import {File} from '@ionic-native/file';

import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

import {Transfer, TransferObject} from "@ionic-native/transfer";

import {UtilServiceProvider} from '../util-service/util-service';


@Injectable()
export class AdminProvider {

  private headers;
  private api;

  constructor(
    private transfer: Transfer,
    public events: Events,
    public storage: Storage,
    public http: Http,
    private auth: AuthServiceProvider,
    public platform: Platform,
    private file: File,
    private alertCtrl: AlertController,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  public getRegisterRequests(status): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = status != 'all' ? {params: {status: status}, headers: this.headers} : {headers: this.headers};
        return this.http.get(this.api + "/admin/requests", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          let data = rsp.response.requests.map(request => {
            var requestDate = moment(new Date(request.updated_at));
            var currentDate = moment();
            var diff = currentDate.diff(requestDate, 'h');
            if (diff > 24) {
              request.updated_show = currentDate.diff(requestDate, 'd') + "d";
            }
            else {
              request.updated_show = diff + "h";
            }
            return request;
          });

          return {success: true, data: data};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }

  public changeRequestStatus(newStatus, requestId): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.patch(this.api + "/admin/requests/status", {
          status: newStatus,
          request_id: requestId
        }, {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public getReports(): Promise<any> {
    debugger;
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.get(this.api + "/admin/reports", {headers: this.headers}).toPromise();
      })
      .then(response => {
        debugger;
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.error || error);
  }


  public getDashboard(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.get(this.api + "/admin/dashboard", {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public getManagerUser(): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return this.http.get(this.api + "/admin/users", {headers: this.headers}).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });

  }


  public register(credentials): Promise<any> {
    return this.http.post(this.api + "/admin/users", credentials, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public Supply2(): Promise<any> {
    return this.http.get(this.api + "/admin/supplies/types", {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response.product_types};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public Supply(Supplyitem): Promise<any> {
    delete Supplyitem.mesaures;
    return this.http.post(this.api + "/admin/supplies", Supplyitem, {headers: this.headers})
      .toPromise()
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


  public getReportCarsByProvider(provider): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {

        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {provider: provider}, headers: this.headers};
        return this.http.get(this.api + "/admin/provider/cars", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public getReportMoneyByProvider(provider): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {

        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {provider: provider}, headers: this.headers};
        return this.http.get(this.api + "/admin/provider/money", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public downloadFileUrl(url): Promise<any> {

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token); //responseType:ResponseContentType.ArrayBuffer
        return this.http.get(this.api + url, {headers: this.headers}).toPromise();
      })
      .then(response => {
        if (response.status == 200) {
          var data = response.arrayBuffer();
          return {success: true, data: data};
        }
        else {
          this.handleError(response.status);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }

  public downloadFile(url, name): Promise<any> {

    let storageDirectory: string = '';

    if (this.platform.is('ios')) {
      storageDirectory = this.file.documentsDirectory;
    }
    else if (this.platform.is('android')) {
      storageDirectory = this.file.externalRootDirectory;
    }

    const fileTransfer: TransferObject = this.transfer.create();

    /*	fileTransfer.onProgress((progressEvent) => {
                  console.log(progressEvent);
                  var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
                  //this.progress = perc;
                  console.log(perc);
                  this.events.publish('onProgressReport',perc);

          });*/

    return this.auth.getSessionToken()
      .then((token) => {
        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        return fileTransfer.download(this.api + url, storageDirectory + name, false, {headers: this.headers});
      })
      .then((entry) => {
        console.log('download complete: ' + entry.toURL());

        const alertSuccess = this.alertCtrl.create({
          title: `Descarga Exitosa!`,
          subTitle: `${name} el archivo se descargó con éxito`,
          buttons: ['Aceptar']
        });

        alertSuccess.present();

        return {success: true, url: entry.toURL()};

      }, err => {
        const alertFailure = this.alertCtrl.create({
          title: `Falló la descarga!`,
          subTitle: `intente nuevamente`,
          buttons: ['Aceptar']
        });

        alertFailure.present();
        return this.handleError(JSON.parse(err.body));
      });
  }

  public getCommuneNames(providerID): Promise<any> {
    return this.auth.getSessionToken()
      .then((token) => {

        this.headers = new Headers();
        this.headers.append('x-access-token', token);
        var options = {params: {provider: providerID}, headers: this.headers};
        return this.http.get(this.api + "/admin/provider-communes", options).toPromise();
      })
      .then(response => {
        let rsp = response.json();
        if (rsp.success) {
          return {success: true, data: rsp.response};
        }
        else {
          this.handleError(rsp);
        }
      })
      .catch(error => {
        return this.handleError(error.json());
      });
  }


}
