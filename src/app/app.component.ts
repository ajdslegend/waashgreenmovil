import {Component, ViewChild} from '@angular/core';

import {Platform, MenuController, Nav, Events, AlertController, LoadingController, Loading} from 'ionic-angular';

import {LoginPage} from '../pages/shared/login/login';

import {RegisterRequestsPage} from '../pages/admin/register-requests/register-requests';
import {ReportsPage} from '../pages/admin/reports/reports';

import {HomePage} from '../pages/admin/home/home';
import {ClientHomePage} from '../pages/client/client-home/client-home';
import {BenefitsPage} from '../pages/client/benefits/benefits';
import {InfoPage} from '../pages/client/info/info';
import {PricePage} from '../pages/client/price/price';


import {TabsPage} from "../pages/admin/tab-manage-users/tab-manage-users";

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {AuthServiceProvider} from '../providers/auth-service/auth-service';

import {clientUpdatePage} from '../pages/client/client-update/client-update';
import {clientCarPage} from '../pages/client/Client-new-car/client-car';
import {ProviderHomePage} from '../pages/provider/provider-home/provider-home';
import {UtilServiceProvider} from '../providers/util-service/util-service';
import {Facebook} from '@ionic-native/facebook';

import {FcmProvider} from '../providers/fcm/fcm';
import {DiscountsPage} from '../pages/discounts/discounts';
import {AdminDiscountsPage} from "../pages/admin/admin-discounts/admin-discounts";

//import  { tap } from 'rxjs/add/operators';

@Component({
  templateUrl: 'app.html'
})
export class WaashGreen {

  @ViewChild(Nav) nav: Nav;

  rootPage = LoginPage;
  pages: Array<{ title: string, logo: string, value: boolean, component: any }>;
  currentUser: any = {profile: {}};
  private api;
  loading: Loading;
  isIos = false;
  isAndroid = false;
  notificationID = "A1"

  constructor(
    public events: Events,
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private auth: AuthServiceProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public util: UtilServiceProvider,
    private fb: Facebook,
    public fcm: FcmProvider) {

    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

    this.initializeApp();
    this.isIos = this.platform.is('ios');
    this.isAndroid = this.platform.is('android');

    this.pages = [];
    this.listenToLoginEvents();

    this.auth.getUserInfo().then((user) => {
      console.log("get user: ")
      console.log(user)
      console.log("\n")
      if (!user) {
        this.enableMenu(false);
        this.logout();
      } else {

        this.currentUser = user;

        //this.nav.setRoot(HomePage, {user:user});
        if (!user.profile) {
          this.enableMenu(false);
          alert("USUARIO NO POSEE PERFIL");
          this.logout();
        }
        else {
          switch (user.profile.role) {
            case  'admin' :
              this.pages = [
                {title: 'Home', logo: 'home', value: true, component: HomePage},
                {title: 'Administrar Usuario', logo: 'people', value: false, component: TabsPage},
                {title: 'Solicitud de Registro', logo: 'clipboard', value: false, component: RegisterRequestsPage},
                {title: 'Reportes', logo: 'albums', value: false, component: ReportsPage},
                {title: 'Cupones', logo: 'trending-down', value: false, component: AdminDiscountsPage}/*,
                { title: 'Respaldar BD', logo: 'download', value:false,component: BackupDatabasePage } */
              ];
              this.enableMenu(true);
              this.nav.setRoot(HomePage);
              break;
            case 'customer' :
              this.pages = [
                {title: 'Inicio', logo: 'home', value: true, component: ClientHomePage},
                {title: 'Nuevo Auto', logo: 'add-circle', value: false, component: clientCarPage},
                {title: 'Editar Perfil', logo: 'create', value: false, component: clientUpdatePage},
                {title: 'Beneficios', logo: 'checkmark', value: false, component: BenefitsPage},
                {title: 'Info', logo: 'information-circle', value: false, component: InfoPage},
                {title: 'Precios', logo: 'pricetag', value: false, component: PricePage},
                {title: 'Cupones', logo: 'trending-down', value: false, component: DiscountsPage}
              ];
              this.nav.setRoot(ClientHomePage);
              this.enableMenu(true);
              break;
            case 'provider' :
              this.pages = [
                {title: 'Inicio', logo: 'home', value: true, component: ProviderHomePage}
              ];
              this.nav.setRoot(ProviderHomePage);
              this.enableMenu(true);
              break;
            default :
              alert("USUARIO NO POSEE ROL DEFINIDO");
              this.logout();
              return;
          }
          this.saveNotificationToken();
        }
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    });
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }

  logout() {
    this.fb.logout();
    this.auth.logout().subscribe(succ => {
      this.nav.setRoot(LoginPage, {show: true});
      this.currentUser = {profile: {}};
      this.enableMenu(false);
    });
  }

  listenToLoginEvents() {

    this.events.subscribe('user:logout', () => {
      this.logout();
    });

    this.events.subscribe('user:login', (user) => {
      this.enableMenu(true);
      console.log("get user:login ")
      console.log(user)
      console.log("\n")
      this.currentUser = user;
    });

    this.events.subscribe('show:loading', () => {
      this.showLoading();
    });

    this.events.subscribe('show:error', (msg) => {
      this.showError(msg);
    });

    this.events.subscribe('show:popup', (title, text) => {
      this.showPopup(title, text);
    });

    this.events.subscribe('hide:loading', () => {
      this.hideLoading();
    })

    this.events.subscribe('openPage', (page) => {
      switch (page) {
        case "requests":
          this.openMenuPage(this.pages[2]);
          break;
        case "customers":
          this.openMenuPage(this.pages[1]);
          break;
        case "reports":
          this.openMenuPage(this.pages[3]);
          break;
        case "home":
          this.openMenuPage(this.pages[0]);
          break;

        case "home-provider":
          this.openMenuPage(this.pages[0]);
          break;

      }
    })

    this.events.subscribe('show:menu', (role) => {
      switch (role) {
        case  'admin' :
          this.pages = [
            {title: 'Home', logo: 'home', value: true, component: HomePage},
            {title: 'Administrar Usuario', logo: 'people', value: false, component: TabsPage},
            {title: 'Solicitud de Registro', logo: 'clipboard', value: false, component: RegisterRequestsPage},
            {title: 'Reportes', logo: 'albums', value: false, component: ReportsPage},
            {title: 'Cupones', logo: 'trending-down', value: false, component: AdminDiscountsPage}/*,
            { title: 'Respaldar BD', logo: 'download', value:false,component: BackupDatabasePage } */
          ];
          this.enableMenu(true);
          break;
        case 'customer' :
          this.pages = [
            {title: 'Inicio', logo: 'home', value: true, component: ClientHomePage},
            {title: 'Nuevo Auto', logo: 'add-circle', value: false, component: clientCarPage},
            {title: 'Editar Perfil', logo: 'create', value: false, component: clientUpdatePage},
            {title: 'Beneficios', logo: 'checkmark', value: false, component: BenefitsPage},
            {title: 'Info', logo: 'information-circle', value: false, component: InfoPage},
            {title: 'Precios', logo: 'pricetag', value: false, component: PricePage},
            {title: 'Cupones', logo: 'trending-down', value: false, component: DiscountsPage}
          ];
          this.enableMenu(true);
          break;
        case 'provider' :
          this.pages = [
            {title: 'Inicio', logo: 'home', value: true, component: ProviderHomePage}
          ];
          this.enableMenu(true);
          break;
        default :
          this.enableMenu(false);
          alert("USUARIO NO POSEE PERFIL");
          this.logout();
          break;
      }
    });

    this.events.subscribe('saveNotificationToken', () => {
      this.saveNotificationToken();
      setTimeout(() => {
        let currentuser: any;
        currentuser = this.currentUser;
        this.currentUser = currentuser;
      }, 100);
    })

  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: false,
      duration: 1000
    });
    this.loading.present();
  }

  hideLoading() {
    if (this.loading != undefined) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

  showError(text) {
    if (!!this.loading)
      this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }

  public openMenuPage(p) {
    this.modificando();
    this.openPage(p);
    p.value = true;
  }

  public modificando() {
    this.pages.forEach(function (p) {
      p.value = false;
    });
  }

  saveNotificationToken() {
    if (this.isIos || this.isAndroid) {
      this.fcm.getAndSaveToken();
      this.fcm.listenToNotifications().subscribe(data => {
        console.log(data);

        this.notificationID = data.id

        if (data.tap) {
          console.log("tap true");

        }

        let ctx: any;
        ctx = this;

        let alert = this.alertCtrl.create({
          title: data.title,
          subTitle: data.body,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                switch (this.notificationID) {
                  case "A1" : //Nueva solicitud de servicio
                    console.log("Nueva solicitud de servicio");
                    if (this.currentUser.profile.role == "admin") {
                      this.nav.setRoot(HomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "A2" : //Servicio tomado
                    console.log("Servicio tomado");
                    if (this.currentUser.profile.role == "admin") {
                      this.nav.setRoot(HomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }
                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "A3" : //Servicio Finalizado
                    console.log("Servicio Finalizado");
                    if (this.currentUser.profile.role == "admin") {
                      this.nav.setRoot(HomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "A4" : //Servicios pasado de "Agendado" a "En Progreso"
                    console.log("Servicios pasado de 'Agendado' a 'En Progreso'");
                    if (this.currentUser.profile.role == "admin") {
                      this.nav.setRoot(HomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "R1" : //Nueva solicitud de servicio
                    console.log("Nueva solicitud de servicio");
                    if (this.currentUser.profile.role == "provider") {
                      this.nav.setRoot(ProviderHomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }
                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "R2" : //Servicio pasado de "Agendado" a "En Progreso"
                    console.log("Servicio pasado de Agendado a En Progreso");
                    if (this.currentUser.profile.role == "provider") {
                      this.nav.setRoot(ProviderHomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};// this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "C1" : //Servicio tomado
                    console.log("Servicio tomado");
                    if (this.currentUser.profile.role == "customer") {
                      this.nav.setRoot(ClientHomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                  case "C2" : //Servicio Finalizado
                    console.log("Servicio Finalizado");
                    if (this.currentUser.profile.role == "customer") {
                      this.nav.setRoot(ClientHomePage);
                    } else {
                      this.currentUser = this.currentUser;//this.currentUser = {profile: {}};//this.logout();
                    }

                    console.log(this.currentUser);
                    // setTimeout(() => {
                    //   this.currentUser = this.currentUser;
                    // }, 100);
                    break;
                }

              }
            }
          ]
        });

        alert.present(prompt);

      }, err => console.log(err));
    }
  }

}
