import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {WaashGreen} from './app.component';

import {LoginPage} from '../pages/shared/login/login';


import {RegisterPage} from '../pages/client/register/register';
import {RegisterWasherPage} from '../pages/provider/register-washer/register-washer';
import {ModalContentPage} from '../pages/provider/register-washer/modal-content';
import {RegisterRequestsPage} from '../pages/admin/register-requests/register-requests';
import {RequestDetailPage} from '../pages/admin/request-detail/request-detail';
import {ManageUsersDetailPage} from '../pages/admin/manage-users-detail/manage-users-detail';

import {DiscountsPage} from '../pages/discounts/discounts'

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {ClientProvider} from '../providers/client/client';
import {AdminProvider} from '../providers/admin/admin';
import {UtilServiceProvider} from '../providers/util-service/util-service';
import {IonicStorageModule} from '@ionic/storage';
import {HttpModule} from '@angular/http';
import {Facebook} from '@ionic-native/facebook'

import {ImageResizer} from '@ionic-native/image-resizer';
/*import { Base64 } from '@ionic-native/base64';*/
/*import { FileChooser } from '@ionic-native/file-chooser';*/
import {File} from '@ionic-native/file';
import {Transfer} from '@ionic-native/transfer';
/*import { FilePath } from '@ionic-native/file-path';*/
import {Camera} from '@ionic-native/camera';
/*import { Base64 } from '@ionic-native/base64';*/
import {AutoCompleteModule} from 'ionic2-auto-complete';
import {CompleteService} from '../providers/complete-service/complete-service';
import {HomePage} from "../pages/admin/home/home";
import {TabsPage} from "../pages/admin/tab-manage-users/tab-manage-users";

import {ReportsPage} from '../pages/admin/reports/reports';
import {ModalListPage} from '../pages/admin/reports/modal-list';
import {ModalDetail} from '../pages/admin/reports/modal-detail';
import {ModalSpentProduct} from '../pages/admin/reports/modal-spent-product';

import {AddAdministratorPage} from "../pages/admin/add-administrator/add-administrator";

import {BackupDatabasePage} from '../pages/admin/backup-database/backup-database';
import {SupplyProviderPage} from "../pages/admin/supply-provider/supply-provider";

import {InAppBrowser} from '@ionic-native/in-app-browser';

import {ClientHomePage} from '../pages/client/client-home/client-home';

import {VehicleSelectionPage} from '../pages/client/vehicle-selection/vehicle-selection';
import {ModeSelectionPage} from '../pages/client/mode-selection/mode-selection';
import {AddressSelectionPage} from '../pages/client/address-selection/address-selection';
import {ClientsinPage} from '../pages/client/client-sin/client-sin';
import {clientCarPage} from '../pages/client/Client-new-car/client-car';
import {historicalDetailPage} from '../pages/client/historical-detail/historical-detail';
import {clientUpdatePage} from '../pages/client/client-update/client-update';
import {PaymentPage} from '../pages/client/payment/payment';

import {BenefitsPage} from '../pages/client/benefits/benefits';
import {InfoPage} from '../pages/client/info/info';
import {PricePage} from '../pages/client/price/price';


import {GoogleMaps, Geocoder} from '@ionic-native/google-maps';
import {Geolocation} from '@ionic-native/geolocation';

import {AndroidPermissions} from '@ionic-native/android-permissions';

import {OrderSummaryPage} from '../pages/client/order-summary/order-summary';
import {Diagnostic} from '@ionic-native/diagnostic';
import {ProviderHomePage} from '../pages/provider/provider-home/provider-home';
import {serviceDetailPage} from '../pages/provider/detail-Service/detail-Service';
import {providerProvider} from '../providers/provider/provider';
import {LocationMapPage} from '../pages/provider/location-map/location-map';
import {serviceDetailinProgressPage} from '../pages/provider/detail-Service-inProgress/detail-Service-inProgress';
import {clientCancelRequestPage} from '../pages/provider/cancel-request/cancel-request';
import {clientCompleteRequestPage} from '../pages/provider/complete-request/complete-request';

import {Keyboard} from '@ionic-native/keyboard';
import {NativeGeocoder} from '@ionic-native/native-geocoder';
import {Firebase} from '@ionic-native/firebase';
import {FcmProvider} from '../providers/fcm/fcm';
import { CouponProvider } from '../providers/coupon/coupon';
import {AdminDiscountsPage} from "../pages/admin/admin-discounts/admin-discounts";

@NgModule({
  declarations: [
    WaashGreen,
    LoginPage,
    RegisterPage,
    RegisterWasherPage,
    RegisterRequestsPage,
    RequestDetailPage,
    ModalContentPage,
    ModalListPage,
    ModalDetail,
    ModalSpentProduct,
    HomePage,
    ClientHomePage,
    OrderSummaryPage,
    VehicleSelectionPage,
    ModeSelectionPage,
    AddressSelectionPage,
    TabsPage,
    ReportsPage,
    ManageUsersDetailPage,
    AddAdministratorPage,
    BackupDatabasePage,
    SupplyProviderPage,
    ClientsinPage,
    clientCarPage,
    historicalDetailPage,
    clientUpdatePage,
    PaymentPage,
    ProviderHomePage,
    serviceDetailPage,
    LocationMapPage,
    serviceDetailinProgressPage,
    clientCancelRequestPage,
    clientCompleteRequestPage,
    BenefitsPage,
    InfoPage,
    PricePage,
    DiscountsPage,
    AdminDiscountsPage
  ],
  imports: [
    BrowserModule,
    AutoCompleteModule,
    HttpModule,
    IonicModule.forRoot(WaashGreen, {
      backButtonText: ''
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    WaashGreen,
    LoginPage,
    RegisterPage,
    RegisterWasherPage,
    RegisterRequestsPage,
    RequestDetailPage,
    ModalContentPage,
    ModalListPage,
    ModalDetail,
    ModalSpentProduct,
    HomePage,
    ClientHomePage,
    OrderSummaryPage,
    VehicleSelectionPage,
    ModeSelectionPage,
    AddressSelectionPage,
    TabsPage,
    ReportsPage,
    ManageUsersDetailPage,
    AddAdministratorPage,
    BackupDatabasePage,
    SupplyProviderPage,
    ClientsinPage,
    clientCarPage,
    historicalDetailPage,
    clientUpdatePage,
    PaymentPage,
    ProviderHomePage,
    serviceDetailPage,
    LocationMapPage,
    serviceDetailinProgressPage,
    clientCancelRequestPage,
    clientCompleteRequestPage,
    BenefitsPage,
    InfoPage,
    PricePage,
    DiscountsPage,
    AdminDiscountsPage
  ],
  providers: [
    InAppBrowser,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    ClientProvider,
    AdminProvider,
    providerProvider,
    Facebook,
    UtilServiceProvider,
    File,
    /*FileChooser,*/
    /*  Base64,*/
    Transfer,
    ImageResizer,
    Camera,
    /* FilePath,*/
    CompleteService,
    GoogleMaps,
    Geocoder,
    Geolocation,
    Diagnostic,
    AndroidPermissions,
    Keyboard,
    NativeGeocoder,
    Firebase,
    FcmProvider,
    CouponProvider
  ]
})
export class AppModule {
}
