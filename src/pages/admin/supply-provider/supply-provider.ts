import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {AdminProvider} from "../../../providers/admin/admin";
import {Validators, FormGroup, FormBuilder} from '@angular/forms';


import {TabsPage} from '../tab-manage-users/tab-manage-users';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-supply-provider',
  templateUrl: 'supply-provider.html',
})
export class SupplyProviderPage {
  FormSupply: FormGroup;
  submitAttempt: boolean = false;
  supplys: any = [];
  mesaures: any = [];
  qtybolean: boolean = false;
  selectedItem: any;
  unregisterBackButtonAction: any;
  private api;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private adminProvider: AdminProvider,
    public formBuilder: FormBuilder,
    public events: Events,
    public util: UtilServiceProvider) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

    this.FormSupply = formBuilder.group(
      {
        qty: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
        product_type: ['', Validators.compose([Validators.required])],
        mesaures: ['',],
        measure: ['',]
      }
    );
    this.selectedItem = navParams.get('item');
    this.Supply();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupplyProviderPage');
  }


  Supply() {
    this.adminProvider.Supply2()
      .then(rsp => {

          if (rsp.success) {
            this.supplys = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error', 'Error al cargar suministros.');
          }
        },
        error => {

          this.events.publish('show:error', !error.description ? 'Error al cargar suministros' : error.description);
        })
      .catch((err) => {

        console.log(err);
        this.events.publish('show:error', 'Error al cargar suministros');
      })
  }

  onChange(value) {
    if (value != undefined && value != "" && value != null) {
      this.mesaures = this.supplys[value].mesaures;
    }
    this.qtybolean = true;
  }

  registersupply() {
    if (!this.FormSupply.valid) {
      this.submitAttempt = true;

    }
    else {
      this.events.publish('show:loading');
      this.FormSupply.value.product_type = this.supplys[this.FormSupply.value.product_type].key;
      this.FormSupply.value.provider = this.selectedItem._id;
      this.FormSupply.value.measure = (this.FormSupply.value.measure == '' || this.FormSupply.value.measure == null) ? this.mesaures[0] : this.FormSupply.value.measure;
      this.adminProvider.Supply(this.FormSupply.value)
        .then(rsp => {
            if (rsp.success) {
              this.events.publish('hide:loading');
              this.events.publish('show:popup', '', 'El Suministro se agregó  éxitosamente.');
              this.mesaures = [];
              this.FormSupply.reset();
              //    this.goToUsers();
            }
            else {
              this.events.publish('show:popup', 'Error', 'Error al agregar suministro.');
            }
          },
          error => {
            this.events.publish('show:error', !error.description ? 'Error al  agregar suministro' : error.description);
          })
        .catch((err) => {
          console.log(err);
          this.events.publish('show:error', 'Error al  agregar suministro');
        })
    }
  }


  ionViewWillLeave() {
    this.goToUsers();
  }

  goToUsers() {
    this.navCtrl.setRoot(TabsPage);
  }
}
