import {Component} from '@angular/core';
import {NavController, NavParams, Events, AlertController} from "ionic-angular";
import {AdminProvider} from "../../../providers/admin/admin";

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-home-detail',
  templateUrl: 'home.html'
})
export class HomePage {
  currentDashboard = {data: {}};
  items = [];
  private api;
  status = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private adminProvider: AdminProvider,
    private events: Events,
    public alertCtrl: AlertController,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  ionViewWillEnter() {
    var ctx = this;
    this.events.publish('hide:loading');
    setTimeout(function () {
      ctx.getdashboard()
    }, 0);
  }

  getdashboard() {
    this.events.publish('show:loading');
    this.adminProvider.getDashboard()

      .then(rsp => {
          if (rsp.success) {
            this.events.publish('hide:loading');
            this.currentDashboard = rsp.data;
            this.items = rsp.data.latest_customer_requests;
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }


  formatearNumero(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
  }

  showStatus(statusEng) {
    switch (statusEng) {
      case "pending":
        return "Pendiente";
      case "finish":
        return "Finalizada";
      case "in_progress":
        return "En Progreso";
      case "canceled":
        return "Cancelada";
      default:
        return "Pendiente";
    }
  }

  openPage(page) {

    switch (page) {
      case "requests":
        this.events.publish('openPage', 'requests');
        break;
      case "customers":
        this.events.publish('openPage', 'customers');
        break;
      case "cars-by-provider":
        this.events.publish('openPage', 'reports');
        break;
      case "money-by-provider":
        this.events.publish('openPage', 'reports');
        break;
    }

  }

  refresh() {
    this.events.publish('openPage', 'home');
  }
}
