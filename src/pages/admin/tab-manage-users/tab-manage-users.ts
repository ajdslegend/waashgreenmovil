import {Component} from '@angular/core';
import {NavController, NavParams, Events, AlertController} from "ionic-angular";
import {AdminProvider} from "../../../providers/admin/admin";
import {ManageUsersDetailPage} from '../manage-users-detail/manage-users-detail';
import 'rxjs/add/operator/map';
import {AddAdministratorPage} from "../add-administrator/add-administrator";
import {SupplyProviderPage} from "../supply-provider/supply-provider";
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

@Component({
  selector: 'page-tab-manage-users',
  templateUrl: 'tab-manage-users.html'
})
export class TabsPage {
  administrar;
  itemsUser = [];
  itemsProvedor = [];
  itemsadmin = [];
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private adminProvider: AdminProvider,
              private events: Events,
              public alertCtrl: AlertController,
              public util: UtilServiceProvider) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.getManagerUser();
    this.administrar = "Cliente";
  }


  getManagerUser() {
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.adminProvider.getManagerUser()

      .then(rsp => {
          if (rsp.success) {

            rsp.data.users.map(request => {
              if (request.profile !== undefined) {
                if (request.profile.role == "customer") {
                  this.itemsUser.push(request)
                } else if (request.profile.role == "admin") {
                  this.itemsadmin.push(request)

                } else {

                  this.itemsProvedor.push(request)

                }

              }


            });
            this.events.publish('hide:loading');
            // this.currentDashboard = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }


  itemSelected(event, item) {
    this.navCtrl.push(ManageUsersDetailPage, {
      item: item
    });
  }

  public createAdmin() {
    this.navCtrl.push(AddAdministratorPage);
  }

  supply(event, item) {
    this.navCtrl.push(SupplyProviderPage, {
      item: item
    });
  }

  refresh() {
    this.events.publish('openPage', 'customers');
  }

}
