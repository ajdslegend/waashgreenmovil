import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';

import {AdminProvider} from "../../../providers/admin/admin";

@Component({
  selector: 'page-manage-users-detail',
  templateUrl: 'manage-users-detail.html',
})
export class ManageUsersDetailPage {
  star1 = false;
  star2 = false;
  star3 = false;
  star4 = false;
  star5 = false;
  public car_liquid: any = 0
  public wheel_liquid: any = 0
  public towel: any = 0
  selectedItem: any;
  private api;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public util: UtilServiceProvider,
    public admin: AdminProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

    this.selectedItem = navParams.get('item');
    debugger;
    this.selectedItem.showStatus = this.showStatus(this.selectedItem.status);

    if (this.selectedItem.supplies && this.selectedItem.supplies.length > 0) {
      this.selectedItem.supplies.map(supp => {
        if (supp.product && supp.product.type) {
          switch (supp.product.type) {
            case "towel" :
              this.towel += supp.qty;
              break;
            case "car_liquid" :
              this.car_liquid += supp.qty;
              break;
            case "wheel_liquid" :
              this.wheel_liquid += supp.qty;
              break;
          }
        }
        this.towel = Math.ceil(this.towel);
        return supp;
      })
    }
    this.showStar(this.selectedItem.profile.rating);
    this.selectedItem.profile.rating = !this.selectedItem.profile.rating ? "" : this.selectedItem.profile.rating;

    this.admin.getCommuneNames(this.selectedItem._id)
      .then(rsp => {
        this.selectedItem.communes = rsp.data.names;
      })
      .catch((err) => {
        console.log("ERROR AL CARGAR NOMBRES DE COMUNSA")
      });

  }

  ionViewDidLoad() {
  }

  showStatus(statusEng) {
    switch (statusEng) {
      case "pending":
        return "Pendiente";
      case "accepted":
        return "Aceptada";
      case "rejected":
        return "Rechazada";
      default:
        return "Todas";
    }
  }

  showStar(star) {
    star = !star ? "none" : star.toString();
    debugger
    switch (star) {
      case "none":
        this.star1 = false;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "1":
        this.star1 = true;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "2":
        this.star1 = true;
        this.star2 = true;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "3":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = false;
        this.star5 = false;
        break;
      case "4":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = true;
        this.star5 = false;
        break;
      case "5":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = true;
        this.star5 = true;
        break;
      default:
        this.star1 = false;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
    }
    debugger
  }

}
