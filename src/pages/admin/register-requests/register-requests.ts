import {Component} from '@angular/core';
import {NavController, NavParams, Events, AlertController} from 'ionic-angular';
import {RequestDetailPage} from '../request-detail/request-detail';
import {AdminProvider} from '../../../providers/admin/admin';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';

@Component({
  selector: 'page-register-requests',
  templateUrl: 'register-requests.html',
})
export class RegisterRequestsPage {
  items = [];
  private api;
  status = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private adminProvider: AdminProvider,
    private events: Events,
    public alertCtrl: AlertController,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  ionViewDidLoad() {

  }

  ionViewWillEnter() {
    var ctx = this;
    setTimeout(function () {
      ctx.getRegisterRequests("pending")
    }, 0);
  }

  itemSelected(event, item) {
    this.navCtrl.push(RequestDetailPage, {
      item: item
    });
  }

  getRegisterRequests(status) {
    this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.adminProvider.getRegisterRequests(status)
      .then(rsp => {
          if (rsp.success) {
            this.events.publish('hide:loading');
            this.items = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }

  showFilter() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filtrar Solicitudes');

    alert.addInput({
      type: 'radio',
      label: 'Pendiente',
      value: 'pending',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Aceptada',
      value: 'accepted',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Rechazada',
      value: 'rejected',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Todas',
      value: 'all',
      checked: false
    });

    alert.addButton({
      text: 'OK',
      handler: status => {
        if (status != "" && status != undefined)
          this.getRegisterRequests(status);
      }
    });
    alert.present();

  }

  showStatus(statusEng) {
    switch (statusEng) {
      case "pending":
        return "Pendiente";
      case "accepted":
        return "Aceptada";
      case "rejected":
        return "Rechazada";
      default:
        return "Todas";
    }
  }

}
