import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDiscountsPage } from './admin-discounts';

@NgModule({
  declarations: [
    AdminDiscountsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDiscountsPage),
  ],
  exports: [
    AdminDiscountsPage
  ]
})
export class AdminDiscountsPageModule {}
