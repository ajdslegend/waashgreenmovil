import {Component} from '@angular/core';
import {AlertController, Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {UtilServiceProvider} from "../../../providers/util-service/util-service";
import {CouponProvider} from "../../../providers/coupon/coupon";

/**
 * Generated class for the AdminDiscountsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-admin-discounts',
  templateUrl: 'admin-discounts.html',
})
export class AdminDiscountsPage {

  model: FormGroup;

  campaign = [];
  coupons = [];
  coupons1 = [];

  autos = [
    {id: 0, nombre: 'TURISMO PEQUEÑO'},
    {id: 1, nombre: 'TURISMO MEDIANO'},
    {id: 2, nombre: 'TURISMO GAMA ALTA'},
    {id: 3, nombre: 'TODO TERRENO'},
    {id: 4, nombre: 'TODOS'}
  ];

  typeServices = [
    {id: 0, nombre: 'EXTERIOR'},
    {id: 1, nombre: 'EXTERIOR + INTERIOR'},
    {id: 2, nombre: 'TODOS'}
  ];

  status = [
    {
      name: "Todos",
      value: "All"
    },
    {
      name: "Disponible",
      value: "available"
    },
    {
      name: "Reservados",
      value: "applied"
    },
    {
      name: "Usado",
      value: "used"
    },
    {
      name: "Vencido",
      value: "defeated"
    }
  ];

  columns=[
    "Usuario",
    "T. Servicios",
    "T. Vehiculo",
    "Status",
    "Fecha"
  ]

  // selectStatus;

  activeForm: boolean = false;

  activeForm1: boolean = false;

  activeCreateBtn = true;

  activeEditBtn = false;

  activeListCoupon: boolean = false;
  ID;

  init_date;
  name_campaign;
  activeListCampaign: boolean = true;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public util: UtilServiceProvider,
              private _coupon: CouponProvider,
              private events: Events,
              public alertCtrl: AlertController,
              private formBuilder: FormBuilder) {
    this.modelForm();
    this.currentDate();
    this.getCampaign();
    // this.getCampaign();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDiscountsPage');
    // this.getCampaign();
    // ^([a-zA-Z0-9+ñÑáéíóúÁÉÍÓÚÀàÈèÌìÒòÙù+,+.+;+:+-+(+)+"+"+'+'+ +a-zA-Z0-9+-]{0,})$
    // /^([a-zA-Z0-9+ñÑáéíóúÁÉÍÓÚÀàÈèÌìÒòÙù+,+.+;+:+-+(+)+"+"+'+'+%+$+ +a-zA-Z0-9+-]{0,})$/
  }

  modelForm() {
    this.model = this.formBuilder.group({
      name_campaign: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^([a-zA-Z0-9+ñÑáéíóúÁÉÍÓÚÀàÈèÌìÒòÙù+,+.+;+:+-+(+)+"+"+\'+\'+ +a-zA-Z0-9+-]{0,})$')])
      ],
      quantity_available: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(1)
      ])],
      begin_date: [null, Validators.required],
      end_date: [null, Validators.required],
      type_vehicle: [null, Validators.required],
      type_service: [null, Validators.required],
      promotion: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(500),
        Validators.minLength(1),
        Validators.pattern('^([a-zA-Z0-9+ñÑáéíóúÁÉÍÓÚÀàÈèÌìÒòÙù+,+.+;+:+-+(+)+"+"+\'+\'+%+$+ +a-zA-Z0-9+-]{0,})$')
      ])],
      discount: [null, Validators.required]

    });
  }

  getCampaign() {
    this.events.publish('show:loading');
    this._coupon.getAdminCampaign('').then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // res.data.map((i) => {
        //   this.data.push(i);
        // });
        this.campaign = res.data;
        this.events.publish('hide:loading');
      } else {
        this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  show_list(i) {
    this.getCoupons('', i._id);
    this.ID = i._id;
    this.name_campaign = i.name_campaign;
    this.activeListCoupon = true;
    this.activeListCampaign = false;
    this.activeForm1 = false;
    this.activeForm = true;
  }

  show_coupons(status) {
    this.getCoupons(status, this.ID);
  }

  getCoupons(status, id) {
    this.events.publish('show:loading');
    this._coupon.getAdminCoupons(status, id).then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // this.coupons = res.data;
        // this.coupons = [res.data[0]];
        if (res.data.length !== 0) {
          this.coupons = [res.data[0]];
          this.coupons1 = res.data;
        } else {
          this.coupons = [];
          this.coupons1 = [];
        }

        this.events.publish('hide:loading');
      } else {
        this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  translateStatus(status) {
    let op: any = "";
    switch (status) {
      case "available":
        op = "Disponible";
        break;
      case "applied":
        op = "Reservados";
        break;
      case "used":
        op = "Usado";
        break;
      case "defeated":
        op = "Vencido";
        break;
    }
    return op;
  }

  edit(i) {
    this.ID = i._id;
    this.model.get('name_campaign').setValue(i.name_campaign);
    this.model.get('promotion').setValue(i.detail.promotion);
    this.model.get('quantity_available').setValue(i.quantity_available);
    // this.model.get('begin_date').setValue(new Date(i.begin_date));
    // this.model.get('end_date').setValue(new Date(i.end_date));
    this.model.get('begin_date').setValue((i.begin_date));
    this.model.get('end_date').setValue((i.end_date));
    this.model.get('type_vehicle').setValue(i.detail.type_vehicle);
    this.model.get('type_service').setValue(i.detail.type_service);
    this.model.get('discount').setValue(i.detail.discount);

    this.activeForm = true;
    this.activeForm1 = true;
    this.activeCreateBtn = false;
    this.activeEditBtn = true;
    this.activeListCoupon = false;
    this.activeListCampaign = false;
  }

  activeCampaign() {
    this.activeForm = true;
    this.activeForm1 = true;
    this.activeListCampaign = false;
    this.activeListCoupon = false;
    this.activeCreateBtn = true;
    this.activeEditBtn = false;
    console.log("#activeCampaign: " + this.activeForm);
  }

  cancelar() {
    this.activeForm = false;
    this.activeForm1 = false;
    this.activeCreateBtn = false;
    this.activeEditBtn = true;
    this.activeListCampaign = true;
    this.activeListCoupon = false;
    console.log("#cancelar: " + this.activeForm);
  }

  addCampaign(form: NgForm) {
    let sendData = {
      name_campaign: form["name_campaign"],
      total_quantity: form["quantity_available"],
      quantity_available: form["quantity_available"],
      old_quantity: 0,
      begin_date: form["begin_date"],
      end_date: form["end_date"],
      promotion: form["promotion"],
      type_vehicle: form["type_vehicle"],
      type_service: form["type_service"],
      discount: form["discount"],
      quantity: form["quantity_available"],
      status: "available"
    }

    this.events.publish('show:loading');
    this._coupon.addAdminCampaign(sendData).then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        this.events.publish('hide:loading');
        this.events.publish('show:popup', '', 'El cupon se agregó  éxitosamente.');
        this.model.reset();
        this.getCampaign();
        this.cancelar();
      } else {
        this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
        this.events.publish('hide:loading');
      }
    }).catch((err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      this.events.publish('hide:loading');
    });
    // this.cancelar();
  }

  updateCampaign(form: NgForm) {
    let sendData = {
      name_campaign: form["name_campaign"],
      total_quantity: form["quantity_available"],
      quantity_available: form["quantity_available"],
      old_quantity: form["quantity_available"],
      begin_date: form["begin_date"],
      end_date: form["end_date"],
      promotion: form["promotion"],
      type_vehicle: form["type_vehicle"],
      type_service: form["type_service"],
      discount: form["discount"],
      quantity: form["quantity_available"],
      status: "available"
    }

    this.events.publish('show:loading');
    this._coupon.updateAdminCampaign(sendData, this.ID).then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        this.events.publish('hide:loading');
        this.events.publish('show:popup', '', 'El cupon se agregó  éxitosamente.');
        this.model.reset();
        this.getCampaign();
        this.cancelar();
      } else {
        this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
        this.events.publish('hide:loading');
      }
    }).catch((err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      this.events.publish('hide:loading');
    });

    //
  }

  showAlert() {
    let id = this.ID;
    // this.activeListCoupon = true;
    //this.events.publish('show:loading');
    this._coupon.getAdminCoupons('', id).then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // this.coupons = res.data;
        let sub = [];
        res.data.map((i) => {

          let date = this.parseDate(i.campaign_id.end_date);
          console.log("#date: " + date + ' ##enddate: ' + i.campaign_id.end_date);
          let target, target1, target2;
          target = {
            name: 'name1',
            type: 'text',
            value: 'E-mail: ' + i.user_id.email
          };

          target1 = {
            name: 'name2',
            type: 'text',
            value: 'T.Servicio: ' + i.campaign_id.detail.type_service
          };

          target2 = {
            name: 'name2',
            type: 'text',
            value: 'T.Vehiculo: ' + i.campaign_id.detail.type_vehicle
          };

          sub.push(target, target1, target2);
        });

        let alert = this.alertCtrl.create({
          title: "Cupón: " + this.name_campaign,
          inputs: sub,
          buttons: ['Aceptar']
        });
        alert.present();
        // this.events.publish('hide:loading');
      } else {
        //this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      //this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      //this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  showAlertUser(arr) {


    let alert = this.alertCtrl.create({
      title: "Cupón: " + this.name_campaign,
      subTitle: '',
      buttons: ['Aceptar']
    });
    alert.present();
  }

  currentDate() {
    let today: any;
    today = new Date();
    let dd: any;
    dd = today.getDate();
    let mm: any;
    mm = today.getMonth() + 1;
    let yyyy: any;
    yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;

    this.init_date = today;
  }

  parseDate(date: number) {
    let today: any;
    // today = new Date(date);
    today = new Date(date);
    let dd: any;
    dd = today.getDate();
    let mm: any;
    mm = today.getMonth() + 1;
    let yyyy: any;
    yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    // today = yyyy + '-' + mm + '-' + dd;
    today = dd + '-' + mm + '-' + yyyy;
    return today;
  }

}
