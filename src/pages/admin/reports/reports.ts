import {Component} from '@angular/core';
import {NavController, NavParams, Events, ModalController, Platform} from 'ionic-angular';

import {ModalListPage} from './modal-list';
import {AdminProvider} from '../../../providers/admin/admin';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

import {ModalDetail} from './modal-detail';

import {File} from '@ionic-native/file';

import {AndroidPermissions} from '@ionic-native/android-permissions';


@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  reports: any = {
    users_distribution: [{count: 0}, {count: 0}, {count: 0}],
    cars_by_providers: [{provider: {profile: {}}, cars: 0}],
    money_by_providers: [{provider: {profile: {}}, money: 0}]
  };
  carsByProviders = [];
  moneyByProviders = [];
  private api;
  isDownload = false;
  totalPerc = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private events: Events,
    private adminProvider: AdminProvider,
    private util: UtilServiceProvider,
    public file: File,
    public platform: Platform,
    private androidPermissions: AndroidPermissions
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    //this.listenEvents();
  }

  ionViewDidLoad() {
    var ctx = this;
    setTimeout(function () {
      ctx.getReportsData()
    }, 0);
  }

  openModal(op) {
    var params = {items: [], op: op};
    params.items = (op == 1) ? this.reports.cars_by_providers : this.reports.money_by_providers;
    let modal = this.modalCtrl.create(ModalListPage, params);
    modal.present();
  }

  openModalDetail(item, op) {
    if (op == 1) {
      this.events.publish('show:loading');
      this.adminProvider.getReportCarsByProvider(item._id)
        .then(rsp => {
            if (rsp.success) { //cars by provider
              this.events.publish('hide:loading');
              item.detail = rsp.data.cars;
              item.detail = item.detail.map((itemCar) => {
                itemCar.date_at = new Date(itemCar.date_at);
                itemCar.date = (itemCar.date_at.toString() != "Invalid Date") ? itemCar.date_at.getDate() + "/" + (itemCar.date_at.getMonth() + 1) + "/" + itemCar.date_at.getFullYear() : "";
                itemCar.hour = (itemCar.date_at.toString() != "Invalid Date") ? this.util.formatHour(itemCar.date_at.getHours(), itemCar.date_at.getMinutes()) : "";

                return itemCar;
              });
              let modal = this.modalCtrl.create(ModalDetail, {item: item, op: op});
              modal.present();

            }
            else {
              this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
            }
          },
          error => {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          })
        .catch((err) => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
    }
    else { // money by providers
      this.events.publish('show:loading');
      this.adminProvider.getReportMoneyByProvider(item._id)
        .then(rsp => {
            if (rsp.success) { //cars by provider
              this.events.publish('hide:loading');
              item.detail = rsp.data.cars;
              let modal = this.modalCtrl.create(ModalDetail, {item: item, op: op});
              modal.present();
            }
            else {
              this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
            }
          },
          error => {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          })
        .catch((err) => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
    }
  }

  getReportsData() {
    this.events.publish('show:loading');
    this.adminProvider.getReports()
      .then(rsp => {
          if (rsp.success) {
            this.events.publish('hide:loading');
            this.reports = rsp.data;
            this.carsByProviders = this.reports.cars_by_providers.slice(0, 3);
            this.moneyByProviders = this.reports.money_by_providers.slice(0, 3);
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }

  downloadReport(type) {
    debugger;
    if (this.platform.is('android')) {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
        result => {
          if (result.hasPermission) {
            this.completeDownload(type)
          } else {
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
          }
        },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      );
    }
    else if (this.platform.is('ios')) {
      this.completeDownload(type);
    }
  }

  completeDownload(type) {
    debugger;
    this.events.publish('show:loading');
    debugger;
    switch (type) {
      case 1 :
        /*this.adminProvider.downloadFileUrl("/admin/reports/distribution")
        .then(rsp => {
          if ( rsp.success ) {
            debugger;
          /  var blob = new Blob([rsp.data], {type: "application/vnd.ms-excel;charset=charset=utf-8"});
            this.file.writeFile(this.file.externalRootDirectory, 'report.xlsx', blob, true)
            .then(success => {
              debugger;
              console.log("Exito");
            },err=> {
              debugger;
              console.log("Exito");
            })
            .catch( (err) => {
              debugger;
              console.log("Exito");
            })
          }
          else {
            alert("Error, pero controlado");
          }
        },
        error => {
          alert("Error");
        })
        .catch( (err) => {
          alert("Error");
        })*/
        this.adminProvider.downloadFile("/admin/reports/distribution", "distributionreport.xlsx")
          .then(rsp => {
              debugger;
              if (rsp.success) {
                console.log("todo bien");
              }
              this.events.publish('hide:loading');
            },
            error => {
              this.events.publish('hide:loading');
            })
          .catch((err) => {
            this.events.publish('hide:loading');
          })
        break;
      case 2 :
        this.adminProvider.downloadFile("/admin/reports/cars-attended", "carsattendedreport.xlsx")
          .then(rsp => {
              debugger;
              if (rsp.success) {
                console.log("todo bien");
              }
              this.events.publish('hide:loading');
            },
            error => {
              this.events.publish('hide:loading');
            })
          .catch((err) => {
            this.events.publish('hide:loading');
          })
        break;
      case 3 :
        //this.util.downloadFileUrl("/admin/reports/money-generated","moneygenerated");
        this.adminProvider.downloadFile("/admin/reports/money-generated", "moneygeneratedreport.xlsx")
          .then(rsp => {
              debugger;
              if (rsp.success) {
                console.log("todo bien");
              }
              this.events.publish('hide:loading');
            },
            error => {
              this.events.publish('hide:loading');
            })
          .catch((err) => {
            this.events.publish('hide:loading');
          })
        break;
    }

  }

  /*  listenEvents() {
      this.events.subscribe('onProgressReport', (perc) => {
        this.isDownload = true;
        this.totalPerc = perc;
        if(perc == 100) {
          var ctx = this;
          setTimeout(function(){this.isDownload = false;},250);
        }
      });
    }*/
}
