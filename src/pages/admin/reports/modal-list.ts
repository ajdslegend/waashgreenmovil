import {Component} from '@angular/core';

import {Platform, NavParams, ViewController, Events, ModalController} from 'ionic-angular';

import {ModalDetail} from './modal-detail';

import {AdminProvider} from '../../../providers/admin/admin';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

@Component({
  selector: 'page-modal-list-page',
  templateUrl: 'modal-list.html'
})
export class ModalListPage {
  items = [];
  title = "";
  op: any;
  public api;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    private events: Events,
    public modalCtrl: ModalController,
    private adminProvider: AdminProvider,
    private util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.items = this.params.get('items');
    this.op = this.params.get('op');
    this.title = this.op == 1 ? "Auto atendidos por proveedor" : "Dinero generado por proveedor";

  }

  openModalDetail(item, op) {
    debugger;
    if (this.op == 1) {
      this.events.publish('show:loading');
      this.adminProvider.getReportCarsByProvider(item._id)
        .then(rsp => {
            if (rsp.success) {
              this.events.publish('hide:loading');
              item.detail = rsp.data.cars;
              item.detail = item.detail.map((itemCar) => {
                itemCar.date_at = new Date(itemCar.date_at);
                itemCar.date = (itemCar.date_at.toString() != "Invalid Date") ? itemCar.date_at.getDate() + "/" + (itemCar.date_at.getMonth() + 1) + "/" + itemCar.date_at.getFullYear() : "";
                itemCar.hour = (itemCar.date_at.toString() != "Invalid Date") ? this.util.formatHour(itemCar.date_at.getHours(), itemCar.date_at.getMinutes()) : "";
                return itemCar;
              });
              let modal = this.modalCtrl.create(ModalDetail, {item: item, op: op});
              modal.present();

            }
            else {
              this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
            }
          },
          error => {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          })
        .catch((err) => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
    }
    else {
      this.events.publish('show:loading');
      this.adminProvider.getReportMoneyByProvider(item._id)
        .then(rsp => {
            if (rsp.success) { //cars by provider
              this.events.publish('hide:loading');
              item.detail = rsp.data.cars;
              let modal = this.modalCtrl.create(ModalDetail, {item: item, op: op});
              modal.present();

            }
            else {
              this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
            }
          },
          error => {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          })
        .catch((err) => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
    }

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


}
