import {Component} from '@angular/core';

import {Platform, NavParams, ViewController} from 'ionic-angular';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'modal-spent-product-page',
  templateUrl: 'modal-spent-product.html'
})
export class ModalSpentProduct {
  itemSelected: any;
  title = "";
  op: any;
  private api;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.itemSelected = this.params.get('item');
    this.title = "Detalle de dinero generado por ";
  }

  ionViewDidLoad() {
    setTimeout(function () {
      var modalContent: any = document.getElementsByTagName("ion-modal");
      modalContent = modalContent[modalContent.length - 1];
      modalContent.style.padding = "30% 16% 16%";
      modalContent.style.background = "rgba(0,0,0,0.5)"
    }, 0);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
