import { Component } from '@angular/core';

import { Platform, NavParams, ViewController, ModalController, Events } from 'ionic-angular';

import { ModalSpentProduct } from  './modal-spent-product';

import { UtilServiceProvider } from '../../../providers/util-service/util-service';

import { AdminProvider } from '../../../providers/admin/admin';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AlertController } from 'ionic-angular';

@Component({
  selector: 'modal-detail-page',
  templateUrl: 'modal-detail.html'
})
export class ModalDetail {
  itemSelected : any;
  title = "";
  op : any;
  private api;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public util: UtilServiceProvider,
    private adminProvider: AdminProvider,
    private events: Events,
    private iab: InAppBrowser,
    public alertCtrl: AlertController

  ) {
    util.getBaseUrl().subscribe((base_url) => { this.api = base_url; });
    this.itemSelected = this.params.get('item');
    this.op = this.params.get('op');
    this.title = this.op == 1 ? "Autos atendidos por " : "Detalle de dinero generado por ";
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  openSpentProductDetail ( item, op ) {
    if ( op == 1 ) {
      let modal = this.modalCtrl.create(ModalSpentProduct, { item:item });
      modal.present();
    }

  }

  downloadReport(providerId,type) {
    this.events.publish('show:loading');
    debugger;
    switch(type) {
      case 1 : //autos atendidos
        this.adminProvider.downloadFile("/admin/reports/cars-attended/"+providerId,"carsattendedreportdetail.xlsx")
        .then(rsp => {
          debugger;
          if ( rsp.success ) {
            console.log("todo bien");
          }
          this.events.publish('hide:loading');
        },
        error => {
          this.events.publish('hide:loading');
        })
        .catch( (err) => {
          this.events.publish('hide:loading');
        })
      break;
      case 2 : //dinero generado
      this.adminProvider.downloadFile("/admin/reports/money-generated/"+providerId,"moneygeneratedreportdatail.xlsx")
      .then(rsp => {
        debugger;
        if ( rsp.success ) {
          console.log("todo bien");
        }
        this.events.publish('hide:loading');
      },
      error => {
        this.events.publish('hide:loading');
      })
      .catch( (err) => {
        this.events.publish('hide:loading');
      })
      break;

    }
  }

  openUrl (url) {
    this.iab.create(url);
  }

  openAlert(msg) {
    let alert = this.alertCtrl.create({
      title: "Observaciones",
      subTitle: msg,
      buttons: ['Aceptar']
    });
    alert.present();
  }

}
