import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {AdminProvider} from '../../../providers/admin/admin';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';

import {InAppBrowser} from '@ionic-native/in-app-browser';


@Component({
  selector: 'page-request-detail',
  templateUrl: 'request-detail.html',
})
export class RequestDetailPage {
  selectedItem: any;
  private api;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private adminProvider: AdminProvider,
    public events: Events,
    public util: UtilServiceProvider,
    private iap: InAppBrowser
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.selectedItem = navParams.get('item');
    this.selectedItem.showStatus = this.showStatus(this.selectedItem.status);
  }

  ionViewDidLoad() {
  }

  showStatus(statusEng) {
    switch (statusEng) {
      case "pending":
        return "Pendiente";
      case "accepted":
        return "Aceptada";
      case "rejected":
        return "Rechazada";
      default:
        return "Todas";
    }
  }

  changeRequestStatus(newStatus) {
    this.events.publish('show:loading');
    this.adminProvider.changeRequestStatus(newStatus, this.selectedItem._id)
      .then(rsp => {
          if (rsp.success) {
            this.events.publish('hide:loading');
            this.events.publish('show:popup', 'Solicitud actualizada satisfactoriamente.');
            this.selectedItem.status = newStatus;
            this.selectedItem.showStatus = this.showStatus(this.selectedItem.status);
          }
          else {
            this.events.publish('hide:loading');
            this.events.publish('show:popup', 'Error, intente nuevamente 1');
          }
        },
        error => {
          this.events.publish('hide:loading');
          this.events.publish('show:popup', 'Error, intente nuevamente 2');
        })
      .catch((err) => {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', 'Error, intente nuevamente 3');
      })
  }

  downloadFile(url) {
    debugger;
    var format = url.split(".");
    format = format[format.length - 1].toLowerCase();
    if (["jpeg", "jpg", "png"].indexOf(format) != -1) {
      this.iap.create(url, "_blank");
    }
    else {
      this.iap.create("https://docs.google.com/viewer?url=" + url, "_blank");
    }
    /*
        this.util.downloadFile(url,"test")
        .then(rsp => {
          debugger;
          console.log(rsp.url);
        },
        error => {
          debugger;
          console.log(error);
          this.events.publish('show:error', 'Error al descargar archivo');
          //this.events.publish('show:error',!error.description ? 'Error al crear cuenta' : error.description);
        })
        .catch( (err) => {
          debugger;
          console.log(err);
          this.events.publish('show:error','Error al descargar archivo');
          //this.events.publish('show:error','Error al crear cuenta');
        })*/
  }


}
