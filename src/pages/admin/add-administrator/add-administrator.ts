import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AdminProvider} from "../../../providers/admin/admin";
import {TabsPage} from "../tab-manage-users/tab-manage-users";

/**
 * Generated class for the AddAdministratorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-administrator',
  templateUrl: 'add-administrator.html',
})
export class AddAdministratorPage {

  OneForm: FormGroup;


  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public events: Events, private adminProvider: AdminProvider) {

    this.OneForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      last_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirm_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    }, {validator: this.matchingPasswords('password', 'confirm_password')});


  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAdministratorPage');
  }


  register() {
    if (!this.OneForm.valid) {
      this.submitAttempt = true;
    }
    else {

      this.submitAttempt = false;
      this.events.publish('show:loading');
      // this.OneForm.value.name = "admin";
      // this.OneForm.value.last_name = "admin";
      this.adminProvider.register(this.OneForm.value)
        .then(rsp => {
            if (rsp.success) {
              this.events.publish('hide:loading');
              this.events.publish('show:popup', '', 'su cuenta ha sido creada con éxito.');
              this.navCtrl.push(TabsPage);
            }
            else {
              this.events.publish('show:popup', 'Error', 'Error al crear cuenta.');
            }
          },
          error => {
            this.events.publish('show:error', !error.description ? 'Error al crear cuenta' : error.description);
          })
        .catch((err) => {
          console.log(err);
          this.events.publish('show:error', 'Error al crear cuenta');
        })

    }
  }


}
