import {Component} from '@angular/core';
import {Events, NavController, NavParams, Platform} from 'ionic-angular';

import {PaymentPage} from '../payment/payment';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';
import {ClientProvider} from '../../../providers/client/client';
import {DiscountsPage} from "../../discounts/discounts";

@Component({
  selector: 'page-order-summary',
  templateUrl: 'order-summary.html',
})
export class OrderSummaryPage {
  docTypesLst: Array<any> = [];
  type: any;
  total: any;
  totalShow: any;
  carType: any;
  hour: any;
  public request: any = {};

  isDiscount = false;
  percentDiscount: number;
  newRequest: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events,
    public platform: Platform,
    private util: UtilServiceProvider,
    private clientProvider: ClientProvider) {
    this.request = this.navParams.get('request');
    this.getDiscount();
    this.type = this.request.mode == "Exterior e Interior" ? "in_and_out" : "ext_only";
    this.carType = this.request.car.type;
    this.total = 0;
    this.totalShow = "0,00";
    this.hour = this.request.is_scheduled ? util.formatHour(this.request.date_at.getHours(), this.request.date_at.getMinutes()) : "";
    this.getBudget(this.request.car._id, this.type);
    this.totalShow = "0,00";
    this.total = 0;
    console.log("OrderSummaryPage");
    console.log("this.request : " + JSON.stringify(this.request));
    console.log("this.newrequest : " + JSON.stringify(this.newRequest));
    this.resumeApp();

  }

  resumeApp() {
    // this.platform.resume.subscribe((result)=>{
    //   console.log('Platform Resume Event');
    //   this.getDiscount();
    //   this.getDiscount2();
    //   this.getBudget(this.request.car._id, this.type);
    //   console.log("this.newrequest : " + JSON.stringify(this.newRequest));
    // });
  }

  ionViewDidLoad() {
    console.log("OrderSummaryPage");
    console.log("this.request : " + JSON.stringify(this.request));
  }

  ionViewWillEnter() {
    console.log("\n");
    console.log("||this.request : " + JSON.stringify(this.request));
    this.getDiscount();
    this.getBudget(this.request.car._id, this.type);
  }

  doPay() {
    this.request.type = this.type;
    this.goToPayment();
  }

  getDiscount() {
    if (this.request.hasOwnProperty("discount")) {
      this.isDiscount = true;
      this.percentDiscount = this.request.discount;
    }
  }

  // getDiscount2(){
  //   try {
  //     if (this.newRequest.isBack) {
  //       // let descuento: any;
  //       // descuento = rsp.total * (this.newRequest.discount / 100);
  //       // total = total - descuento;
  //
  //       this.isDiscount = true;
  //       this.percentDiscount = this.newRequest.discount;
  //
  //       this.request.coupon_code = this.newRequest.coupon_code;
  //       this.request.discount = this.newRequest.discount;
  //
  //       // console.log("#newRequest-->discount: #total: " + total);
  //     }
  //   }catch (e) {
  //     console.log("#newRequest-->err: " + e.message);
  //   }
  // }

  goToPayment() {
    this.navCtrl.push(PaymentPage, {
      request: this.request
    });
  }

  navigateToCoupon() {
    let oldCp, newCp;
    oldCp = "OrderSummaryPage";
    newCp = "DiscountsPage";
    let payload = {
      "next": oldCp,
      "isNext": true,
      "dataNext": this.request,
      "back": newCp
    };
    this.navCtrl.push(DiscountsPage, {
      pgRequest: payload
    });
  }

  getBudget(cardId: String, type: String) {
    let total = 0;
    this.clientProvider.getBudget(cardId, type)
      .then((rsp: any) => {
          this.events.publish('hide:loading');
          if (rsp.success) {
            // this.total = rsp.total;
            // if (this.request.hasOwnProperty("discount")) {
            //   let descuento: any;
            //   descuento = rsp.total * (this.request.discount / 100);
            //   this.total = this.total - descuento;
            // }
            total = total + rsp.total;
            this.total = this.util.formatearNumero(total);
            console.log("#|#total: " + total);
            if (this.request.hasOwnProperty("discount")) {
              let descuento: any;
              descuento = rsp.total * (this.request.discount / 100);
              total = total - descuento;
              console.log("#|request-->discount: #total: " + total);
            }

            console.log("#*****#total: " + total);
            this.totalShow = this.util.formatearNumero(total);
            if (this.totalShow.indexOf(",") == -1) {
              this.totalShow += ",00";
            }
          }
          else {
            this.events.publish('show:popup', 'Error', 'Error al calcular presupuesto, intente nuevamente');
          }
        },
        error => {
          this.events.publish('hide:loading');
          this.events.publish('show:popup', 'Error', 'Error al calcular presupuesto, intente nuevamente');
        });
  }

}
