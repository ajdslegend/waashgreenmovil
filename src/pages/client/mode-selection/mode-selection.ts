import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

import {AddressSelectionPage} from '../address-selection/address-selection';
import {AlertController} from 'ionic-angular';
import {CouponProvider} from "../../../providers/coupon/coupon";


@Component({
  selector: 'page-mode-selection',
  templateUrl: 'mode-selection.html',
})
export class ModeSelectionPage {

  public showDate = false;
  public alertMsg = [["Exterior", "Exterior e interior"], ["<ul><li style='text-align: justify;'>Lavado exterior en seco con cera.</li><li style='text-align: justify;'>Limpieza con paños de microfibra.</li><li style='text-align: justify;'>Renovador de neumáticos.</li><li style='text-align: justify;'>Limpieza de llantas.</li></ul>", "Exterior:<br><ul><li style='text-align: justify;'>Lavado exterior en seco con cera.</li><li style='text-align: justify;'>Limpieza con paños de microfibra.</li><li style='text-align: justify;'>Renovador de neumáticos.</li><li style='text-align: justify;'>Limpieza de llantas.</li></ul>Interior:<br><ul><li style='text-align: justify;'>Limpieza interior y aspirado.</li><li style='text-align: justify;'>Limpieza de pisos de goma.</li></ul>"]]
  public today = new Date();
  public showDateMin = "";
  public showDateMax = "";
  public myDate = "";
  public myHour = "08:30";
  public moment: any = "inmediato";
  public type: any = "ext_only";
  public extraComments = "";
  public isCoupon = false;
  public listCoupon = [];
  public coupon_code: any = undefined;
  public id_coupon: any = undefined;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _coupon: CouponProvider,
    public alertCtrl: AlertController) {
    this.showDateMin = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
    if ((this.today.getMonth() + 1).toString().length == 1 && this.today.getDate().toString().length == 1) {
      this.showDateMin = this.today.getFullYear() + '-' + '0' + (this.today.getMonth() + 1) + '-' + '0' + this.today.getDate();
    }
    else if ((this.today.getMonth() + 1).toString().length == 1 && this.today.getDate().toString().length > 1) {
      this.showDateMin = this.today.getFullYear() + '-' + '0' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
    }
    else if ((this.today.getMonth() + 1).toString().length > 1 && this.today.getDate().toString().length == 1) {
      this.showDateMin = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + '0' + this.today.getDate();
    }
    else {
      this.showDateMin = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
    }
    this.myDate = this.showDateMin;
    this.today.setTime(this.today.getTime() + 30 * 24 * 60 * 60 * 1000);
    if ((this.today.getMonth() + 1).toString().length == 1 && this.today.getDate().toString().length == 1) {
      this.showDateMax = this.today.getFullYear() + '-' + '0' + (this.today.getMonth() + 1) + '-' + '0' + this.today.getDate();
    }
    else if ((this.today.getMonth() + 1).toString().length == 1 && this.today.getDate().toString().length > 1) {
      this.showDateMax = this.today.getFullYear() + '-' + '0' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
    }
    else if ((this.today.getMonth() + 1).toString().length > 1 && this.today.getDate().toString().length == 1) {
      this.showDateMax = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + '0' + this.today.getDate();
    }
    else {
      this.showDateMax = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
    }

    this.valueTypeDefault();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModeSelectionPage');
  }

  next() {
    let request = this.navParams.get('request');
    request.type = this.type;
    request.extra_comments = this.extraComments;
    if (this.moment == "agendar") {
      request.moment = this.myDate;
      request.date_at = new Date(parseInt(this.myDate.split("-")[0]), parseInt(this.myDate.split("-")[1]) - 1, parseInt(this.myDate.split("-")[2]), parseInt(this.myHour.split(":")[0]), parseInt(this.myHour.split(":")[1]));
      request.is_scheduled = true;
    }
    else {
      request.moment = "Inmediato";
      request.date_at = new Date();
      request.is_scheduled = false;
    }

    if (request.type == "ext_only") {
      request.mode = "Solo Exterior";
    }
    else {
      request.mode = "Exterior e Interior";
    }
    request.coordinates = [-33.4372, -70.6506];
    request.address = "Plaza de Armas 960, Santiago, Región Metropolitana, Chile";
    request.commune = "santiago";
    request.indications = "Piso 5";
    let val = undefined;
    let val1 = undefined;
    if (this.id_coupon === "undefined") {
      console.log("#/this.coupon_code: " + val);
      this.coupon_code = val;
      this.id_coupon = val1;
    } else if (this.id_coupon !== undefined) {
      // console.log("#this.coupon_code: " + this.coupon_code);
      let discount;
      let couponcode;
      let arr_coupon = [];
      this.listCoupon.map((i) => {
        if (i._id === this.id_coupon) {
          discount = i.campaign_id.detail.discount;
          couponcode = i.coupon_code;
          arr_coupon = [{coupon_code: couponcode, discount: discount}];
        } else {
          discount = undefined;
          couponcode = undefined;
        }

        console.log("#//##discount: " + discount + " ##coupon_code: " + couponcode);
      });

      try {
        request.discount = arr_coupon[0].discount;
        request.coupon_code = arr_coupon[0].coupon_code;
        request.id_coupon = this.id_coupon;
      } catch (e) {
      }
    }

    this.navCtrl.push(AddressSelectionPage, { //OrderSummaryPage
      request: request
    });
  }

  showDateF(value) {
    this.showDate = value;
  }

  showAlert(op) {
    let alert = this.alertCtrl.create({
      title: this.alertMsg[0][op],
      subTitle: this.alertMsg[1][op],
      buttons: ['Aceptar']
    });
    alert.present();
    let option = "";

    console.log('##op: --> ' + op);
    switch (op) {
      case 0:
        option = "exterior";
        break;
      case 1:
        option = "exterior + interior";
        break;
    }

    console.log('##option: --> ' + option);

    this.getCouponApplied(option);
  }

  valueTypeDefault() {
    let types: any;
    types = this.type;
    let option = "";

    console.log('##op: --> ' + types);
    switch (types) {
      case "ext_only":
        option = "exterior";
        break;
      case "in_and_out":
        option = "exterior + interior";
        break;
    }

    console.log('##option: --> ' + option);

    this.getCouponApplied(option);
  }


  // type_service
  getCouponApplied(op) {
    let payload = {
      "type_service": op
    };
    this._coupon.getCouponsApplied(payload).then((res) => {
      if (res.success === true) {
        this.listCoupon = res.data;
        if (this.listCoupon.length !== 0) {
          this.isCoupon = true;
        } else {
          this.isCoupon = false;
          this.coupon_code = undefined;
        }
      }
    });
  }

  selectCopupon(i, evt) {
    // console.log('##i: ' + i + ' #evt: ' + evt.checked);
    // this.coupon_code = undefined;
    let val = undefined;
    let val_id = undefined;
    if (evt.checked === true) {
      val = i.coupon_code;
      val_id = i._id;
      console.log('##i: ' + this.coupon_code + ' #evt: ' + evt.checked);
    } else {
      val = undefined;
      val_id = undefined;
      console.log('##i: ' + this.coupon_code + ' #evt: ' + evt.checked);
    }
    this.coupon_code = val;
    this.id_coupon = val_id;
  }

}
