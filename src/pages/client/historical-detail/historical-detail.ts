import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {FormBuilder} from '@angular/forms';
import {ClientProvider} from '../../../providers/client/client';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';

@Component({
  selector: 'page-historical-detail',
  templateUrl: 'historical-detail.html',
})
export class historicalDetailPage {
  star1 = false;
  star2 = false;
  star3 = false;
  star4 = false;
  star5 = false;
  tipo = "none";
  ratev = 0;
  selectedItem: any;
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public events: Events,
              private clientProvider: ClientProvider,
              util: UtilServiceProvider) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.selectedItem = navParams.get('item');
    this.selectedItem.car_detail.image_url = (!this.selectedItem.car_detail.image_url || this.selectedItem.car_detail.image_url == "") ? "assets/img/mycar.png" : this.api + this.selectedItem.car_detail.image_url;
    this.selectedItem.car_detail.model = !this.selectedItem.car_detail.model.name ? {} : this.selectedItem.car_detail.model;
    this.showStar(this.selectedItem.rate);
    this.showType(this.selectedItem.type);
  }


  showStar(star) {
    star = star.toString();
    switch (star) {
      case "0":
        this.star1 = false;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "1":
        this.star1 = true;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "2":
        this.star1 = true;
        this.star2 = true;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
      case "3":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = false;
        this.star5 = false;
        break;
      case "4":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = true;
        this.star5 = false;
        break;
      case "5":
        this.star1 = true;
        this.star2 = true;
        this.star3 = true;
        this.star4 = true;
        this.star5 = true;
        break;
      default:
        this.star1 = false;
        this.star2 = false;
        this.star3 = false;
        this.star4 = false;
        this.star5 = false;
        break;
    }
  }


  showType(type) {
    switch (type) {
      case "in_and_out":
        this.tipo = "Lavado Exterior e Interior";
        break;
      case "ext_only":
        this.tipo = "Lavado Exterior";
        break;
      default:
        this.tipo = "Por Asignar";
        break;
    }
  }

  rate(type) {
    if (this.selectedItem.rate === 0) {
      switch (type) {
        case 1:
          this.ratev = 1;
          break;
        case 2:
          this.ratev = 2;
          break;
        case 3:
          this.ratev = 3;
          break;
        case 4:
          this.ratev = 4;
          break;
        case 5:
          this.ratev = 5;
          break;
        default:
          this.ratev = 1;
          break;
      }
      this.showStar(type.toString());
    }
  }


  requestRate(event) {
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.clientProvider.requestRate(this.ratev, this.selectedItem._id, this.selectedItem.provider._id)

      .then(rsp => {
          if (rsp.success) {
            this.selectedItem.rate = this.ratev;
            this.events.publish('hide:loading');
            this.events.publish('show:popup', rsp.data);

          }
          else {
            this.events.publish('hide:loading');
            this.events.publish('show:popup', 'Error al calificar , intente nuevamente');
          }
        },
        error => {
          this.events.publish('hide:loading');
          this.events.publish('show:popup', 'Error al calificar , intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', 'Error al calificar , intente nuevamente');
      })

  }


}
