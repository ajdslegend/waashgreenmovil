import {Component} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {ClientProvider} from '../../../providers/client/client';
import {Camera} from "@ionic-native/camera";
import {UtilServiceProvider} from '../../../providers/util-service/util-service';
import {AuthServiceProvider} from '../../../providers/auth-service/auth-service';

import {ClientHomePage} from '../client-home/client-home';
import {ImageResizer} from '@ionic-native/image-resizer';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'page-client-car',
  templateUrl: 'client-car.html',
})
export class clientCarPage {
  FormCar: FormGroup;
  submitAttempt: boolean = false;
  qtybolean: boolean = false;
  selectedItem: any;
  itemsCarsbrands = [];
  itemscarmodels = [];
  Image_avatar: any = 'assets/img/img-empty.JPG';
  public currentModel = 0;
  public currentBrand = 0;
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private events: Events,
              private clientProvider: ClientProvider,
              private camera: Camera,
              private util: UtilServiceProvider,
              private auth: AuthServiceProvider,
              imageResizer: ImageResizer,
              public _DomSanitizationService: DomSanitizer) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.getCarBrands();
    this.FormCar = formBuilder.group(
      {
        license_plate: ['', Validators.compose([Validators.required])],
        alias: ['', Validators.compose([Validators.required])],
        brand: ['', Validators.compose([Validators.required])],
        model: ['', Validators.compose([Validators.required])],
        type: ['', Validators.compose([Validators.required])],
        doors: ['', Validators.compose([Validators.required])],
        color: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])]
      }
    );
  }

  sanitizerImg(img) {
    try {
      // return this._DomSanitizationService.bypassSecurityTrustResourceUrl(img)
      return this._DomSanitizationService.bypassSecurityTrustResourceUrl(img);
    } catch (e) {

    }

  }

  getCarBrands() {
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.clientProvider.getcarsBrands()

      .then(rsp => {
          if (rsp.success) {
            rsp.data.map(request => {

              this.itemsCarsbrands.push(request)

            });
            this.events.publish('hide:loading');
            // this.currentDashboard = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error al cargar modelos, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar modelos, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar modelos, intente nuevamente');
      })
  }


  onChange(value) {
    this.qtybolean = true;
    if (value != undefined && value != "") {
      this.itemscarmodels = this.itemsCarsbrands[value].car_models;
    }
  }


  registercar() {

    debugger;
    if (!this.FormCar.valid) {
      this.submitAttempt = true;
    }
    else if (this.Image_avatar == 'assets/img/img-empty.JPG') {
      this.events.publish('show:error', "Falta imágen del vehículo");
    }
    else {
      this.submitAttempt = false;
      this.events.publish('show:loading');
      this.currentModel = this.FormCar.value.model;
      this.currentBrand = this.FormCar.value.brand;
      this.FormCar.value.model = this.itemscarmodels[this.currentModel]._id;
      this.FormCar.value.brand = this.itemsCarsbrands[this.currentBrand]._id;
      let values = this.FormCar.value;

      var options = {
        params: {
          name: this.FormCar.value.alias,
          rut: ''
        }
      };
      this.auth.getUserInfo()
        .then((user) => {
          options.params.rut = user.email;
          return this.util.uploadFile(this.Image_avatar, options);
        })
        .then(imageUrl => {
          values.image_url = imageUrl.data;
          return this.clientProvider.registercars(values);
        })
        .then(rsp => {
            if (rsp.success) {
              this.events.publish('hide:loading');
              this.events.publish('show:popup', '', 'El Auto se agrego  éxitosamente.');
              this.FormCar.reset();
              this.Image_avatar = 'assets/img/img-empty.JPG';
              this.goToHome();
            }
            else {
              this.events.publish('show:popup', 'Error', 'Error al agregar Auto.');
            }
          },
          error => {
            this.events.publish('show:error', !error.description ? 'Error al  agregar Auto' : error.description);
            this.FormCar.value.model = this.currentModel;
            this.FormCar.value.brand = this.currentBrand;
          })
        .catch((err) => {
          console.log(err);
          this.events.publish('show:error', 'Error al  agregar Auto');
          this.FormCar.value.model = this.currentModel;
          this.FormCar.value.brand = this.currentBrand;
        })
    }
  }

  accessGallery() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      quality: 40,
      targetWidth: 1280,
      targetHeight: 1280,
    }).then((imageData) => {
      this.Image_avatar = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });

  }

  goToHome() {
    this.navCtrl.setRoot(ClientHomePage);
  }
}
