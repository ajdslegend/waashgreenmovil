import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {InAppBrowser} from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iab: InAppBrowser) {

  }

  ionViewDidLoad() {
  }

  openUrl(url) {
    this.iab.create(url);
  }

}
