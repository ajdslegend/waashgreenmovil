import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

import { AuthServiceProvider } from '../../../providers/auth-service/auth-service';
import { UtilServiceProvider } from '../../../providers/util-service/util-service';
import { ClientProvider } from '../../../providers/client/client';

@Component({
  selector: 'page-client-update',
  templateUrl: 'client-update.html',
})
export class clientUpdatePage {
  FormProfile: FormGroup;
  submitAttempt: boolean = false;
  selectedItem: any;
  currentUser :any;

  private api;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public events: Events,
    private auth: AuthServiceProvider,
    public util: UtilServiceProvider,
    private clientProvider: ClientProvider
  ) {

      util.getBaseUrl().subscribe((base_url) => { this.api = base_url; });
      this.FormProfile = formBuilder.group({
        email: [{value: '', disabled: true}, Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required])],
        last_name: ['',Validators.compose([Validators.required])],
        phone:  ['',Validators.compose([Validators.required])]
      });

      this.auth.getUserInfo().then((user) => {
        if(!!user) {
          this.FormProfile.controls['email'].setValue(user.email);
          this.FormProfile.controls['name'].setValue(user.profile.name);
          this.FormProfile.controls['last_name'].setValue(user.profile.last_name);
          this.FormProfile.controls['phone'].setValue(user.profile.phone);
          this.currentUser = user;
        }

      });
  }

  editProfile () {
    if (!this.FormProfile.valid) {
      this.submitAttempt = true;
    }
    else{

      this.clientProvider.changeProfile(this.FormProfile.value)
      .then(rsp => {
        if ( rsp.success ) {
          this.events.publish('show:popup', '', 'Datos Actualizados exitosamente.');

          this.currentUser.profile.name = this.FormProfile.value.name;
          this.currentUser.profile.last_name = this.FormProfile.value.last_name;
          this.currentUser.profile.phone = this.FormProfile.value.phone;

          this.auth.updateUser(this.currentUser).subscribe(succ => {
            console.log(succ);
      		});

        }
        else {
          this.events.publish('show:popup','Error al cargar modelos, intente nuevamente');
        }
      },
      error => {
        this.events.publish('show:popup','Error al cargar modelos, intente nuevamente');
      })
      .catch( (err) => {
        this.events.publish('show:popup','Error al cargar modelos, intente nuevamente');
      });
    }
  }


}
