import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {clientCarPage} from '../Client-new-car/client-car';


@Component({
  selector: 'page-client-sin',
  templateUrl: 'client-sin.html',
})
export class ClientsinPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events) {

  }

  ionViewWillEnter() {
    this.events.publish('hide:loading');
  }

  itemSelected(event, item) {
    this.navCtrl.push(clientCarPage, {
      item: item
    });
  }

}
