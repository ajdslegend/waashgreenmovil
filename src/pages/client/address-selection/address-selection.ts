import {
  BaseArrayClass,
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  MarkerOptions,
  Marker,
  LatLng,
  Geocoder,
  GeocoderRequest,
  GeocoderResult
} from '@ionic-native/google-maps';
import {Component, ViewChild, ElementRef} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {Geolocation, Geoposition} from '@ionic-native/geolocation';

import {OrderSummaryPage} from '../order-summary/order-summary';

import {Diagnostic} from '@ionic-native/diagnostic';
import {AlertController} from 'ionic-angular';

import {ClientProvider} from '../../../providers/client/client';

import {NativeGeocoder} from '@ionic-native/native-geocoder';

@Component({
  selector: 'page-address-selection',
  templateUrl: 'address-selection.html',
})
export class AddressSelectionPage {
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  myMarker: Marker;
  geocoderRequest: GeocoderRequest;
  markerOptions: MarkerOptions;
  // 10.5113,-66.8980766
  latlng = {lat: 0.0, lng: 0.0};
  // latlng = {lat: 10.5113, lng: -66.8980766};
  public address = "";
  public commune = "";
  public indications = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events,
    googleMaps: GoogleMaps,
    private geolocation: Geolocation,
    private diagnostic: Diagnostic,
    public alertCtrl: AlertController,
    private clientProvider: ClientProvider,
    private nativeGeocoder: NativeGeocoder) {
  }

  ionViewDidLoad() {
    this.currentLocation();
    // this.defaultLocation();
  }

  ///// esto es para prueba
  defaultLocation() {
    this.events.publish('show:loading');
    this.diagnostic.isLocationEnabled().then((isAvailable) => {
      console.log('Is available? ' + isAvailable);
      if (isAvailable) {
        this.loadMap({lat: -33.44647, lng: -70.64386});
      } else {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', "Por favor active el GPS para poder continuar");
        this.navCtrl.pop();
      }
    }).catch((e) => {
      this.events.publish('hide:loading');
      this.events.publish('show:popup', "Por favor active el GPS para poder continuar");
      this.navCtrl.pop();
    });
  }

  //// esto es para produccion
  currentLocation() {
    this.events.publish('show:loading');
    this.diagnostic.isLocationEnabled().then((isAvailable) => {
      console.log('Is available? ' + isAvailable);
      if (isAvailable) {
        this.geolocation.getCurrentPosition().then((position: Geoposition) => {
          this.loadMap({lat: position.coords.latitude, lng: position.coords.longitude});
        }, err => {
          console.log("error en geolocation");
          this.events.publish('hide:loading');
          this.events.publish('show:popup', "No se pudo obtener su ubicación actual, por favor intente nuevamente");
          this.navCtrl.pop();
        })
      }
      else {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', "Por favor active el GPS para poder continuar");
        this.navCtrl.pop();
      }
    }).catch((e) => {
      this.events.publish('hide:loading');
      this.events.publish('show:popup', "Por favor active el GPS para poder continuar");
      this.navCtrl.pop();
    });
  }

  loadMap(currentLocation) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: currentLocation,
        zoom: 17,
        tilt: 27
      },
      controls: {
        zoom: true
      }
    };
    this.map = GoogleMaps.create(this.mapElement.nativeElement, mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        let markerOptions: MarkerOptions = {
          position: currentLocation,
          title: "Dirección para el lavado"
        };
        this.events.publish('hide:loading')
        this.addMarker(markerOptions);
        this.map.setMyLocationEnabled(true);
        this.map.setCompassEnabled(true);
      });
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(latLng => {
      this.map.clear().then(() => {
        let markerOptions: MarkerOptions = {
          position: latLng[0],
          title: "Dirección para el lavado"
        };
        this.addMarker(markerOptions);
      });
    });
  }

  addMarker(options, doGeocoder = true) {
    let markerOptions: MarkerOptions = {
      position: new LatLng(options.position.lat, options.position.lng),
      title: options.title,
      icon: 'blue',
      animation: 'DROP'
    };
    this.map.addMarker(markerOptions).then(marker => {
      marker.showInfoWindow();
    });
    this.latlng.lat = options.position.lat;
    this.latlng.lng = options.position.lng;
    this.map.setCameraTarget(new LatLng(options.position.lat, options.position.lng));
    if (doGeocoder) {
      this.events.publish('show:loading');
      this.nativeGeocoder.reverseGeocode(options.position.lat, options.position.lng)
      // .then((result: NativeGeocoderReverseResult) => {
        .then((result: any) => {
          if (result[0] != undefined) {
            this.commune = result[0].locality.trim().toLowerCase();
            this.checkRegion(this.commune);
            this.address = "";
            this.address += result[0].thoroughfare ? result[0].thoroughfare + " " : "";
            this.address += result[0].subThoroughfare ? result[0].subThoroughfare + ", " : ""
            this.address += result[0].locality ? result[0].locality + ", " : ""
            this.address += result[0].administrativeArea ? result[0].administrativeArea + ", " : ""
            this.address += result[0].countryName ? result[0].countryName : ""
          }
          else {
            this.events.publish('hide:loading');
            this.events.publish('show:popup', 'Error', 'El servicio no esta disponible para la región seleccionada');
          }
        })
        .catch((error: any) => {
          console.log(error);
          this.events.publish('hide:loading');
          this.events.publish('show:popup', 'Error', 'El servicio no esta disponible para la región seleccionada');
        });


    }

  }

  next() {
    if (this.commune == "") {
      this.events.publish('show:popup', 'Error', 'El servicio no esta disponible para la región seleccionada');
      return;
    }
    let request = this.navParams.get('request');
    request.coordinates = [this.latlng.lat, this.latlng.lng];
    request.address = this.address;
    request.indications = this.indications;
    request.commune = this.commune;
    this.navCtrl.push(OrderSummaryPage, {
      request: request
    });
  }

  searchAddr() {
    let addr = this.address;
    if (addr != "") {
      this.events.publish('show:loading');

      this.geocoderRequest = {address: addr};
      Geocoder.geocode(this.geocoderRequest).then((geocoderResults: GeocoderResult[] | BaseArrayClass<GeocoderResult>) => {
        this.events.publish('hide:loading');
        //console.log(JSON.stringify(geocoderResults));
        if (!!geocoderResults[0] && !!geocoderResults[0].position) {
          this.latlng.lat = geocoderResults[0].position.lat;
          this.latlng.lng = geocoderResults[0].position.lng;

          this.map.clear().then(() => {
            let markerOptions: MarkerOptions = {
              position: this.latlng,
              title: "Dirección para el lavado"
            };
            this.addMarker(markerOptions, true);
          });

        }
        else {
          let alert = this.alertCtrl.create({
            title: "Sin resultados",
            subTitle: "No se encontraron resultados para la dirección indicada",
            buttons: ['Aceptar']
          });
          alert.present();
        }
      }, err => {
        console.log(err);
        this.events.publish('hide:loading');
        let alert = this.alertCtrl.create({
          title: "Sin resultados",
          subTitle: "No se encontraron resultados para la dirección indicada",
          buttons: ['Aceptar']
        });
        alert.present();
      });
    }
  }

  checkRegion(commune: String) {
    this.clientProvider.checkRegion(commune)
      .then((rsp: any) => {
          this.events.publish('hide:loading');

          if (rsp.success) {
          }
          else {
            this.events.publish('show:popup', 'Error', 'El servicio no esta disponible para la comuna seleccionada');
            this.commune = "";
          }
        },
        error => {
          this.events.publish('hide:loading');

          this.events.publish('show:popup', 'Error', 'El servicio no esta disponible para la comuna seleccionada');
          this.commune = "";
        });

  }


}
