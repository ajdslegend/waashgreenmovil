import {Component, ViewChild, HostListener} from '@angular/core';
import {NavController, Events} from 'ionic-angular';
import {AuthServiceProvider} from '../../../providers/auth-service/auth-service';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';

import {ClientHomePage} from '../../client/client-home/client-home';
import {ProviderHomePage} from '../../provider/provider-home/provider-home';
import {HomePage} from '../../admin/home/home';

import {Keyboard} from '@ionic-native/keyboard';
import {InAppBrowser} from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('signupSlider') signupSlider: any;
  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;

  submitAttempt: boolean = false;
  submitAttempt2: boolean = false;


  constructor(
    public nav: NavController,
    public formBuilder: FormBuilder,
    private auth: AuthServiceProvider,
    public events: Events,
    private fb: Facebook,
    public keyboard: Keyboard,
    private iab: InAppBrowser
  ) {

    this.slideOneForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      last_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      phone: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
      region: [''],
      communes: [''],
      agree: [false, Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirm_password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    }, {validator: this.matchingPasswords('password', 'confirm_password')});

  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  ionViewDidLoad() {

    this.signupSlider.onlyExternal = true;

  }


  next() {
    this.signupSlider.update();
    this.signupSlider.slideNext();
  }

  prev() {
    this.signupSlider.update();
    this.signupSlider.slidePrev();
  }

  register() {
    if (!this.slideOneForm.valid) {

      if (this.slideOneForm.get('name').errors || this.slideOneForm.get('last_name').errors ||
        this.slideOneForm.get('phone').errors || this.slideOneForm.get('email').errors) {
        this.submitAttempt2 = false;
        this.submitAttempt = true;
        this.signupSlider.slideTo(0);
      }
      else {
        this.submitAttempt = false;
        this.submitAttempt2 = true;
        this.signupSlider.slideTo(1);
      }

    }
    else {

      this.submitAttempt = false;
      this.events.publish('show:loading');
      if (this.slideOneForm.value.agree) {
        this.slideOneForm.value.role = "customer";
        this.slideOneForm.value.rut = "1234567890"; //TODO: quitar esto cuando se cambie el endpoint de register
        this.auth.register(this.slideOneForm.value)
          .then(rsp => {
              if (rsp.success) {
                this.events.publish('hide:loading');
                document.getElementById('content-sign-up').style.display = 'none';
                document.getElementById('footer').style.display = 'none';
                document.getElementById('header').style.display = 'none';
                document.getElementById('content-check').style.display = '';
              }
              else {
                this.events.publish('show:popup', 'Error', 'Error al crear cuenta.');
              }
            },
            error => {
              this.events.publish('show:error', !error.description ? 'Error al crear cuenta' : error.description);
            })
          .catch((err) => {
            console.log(err);
            this.events.publish('show:error', 'Error al crear cuenta');
          })
      }
      else {
        this.events.publish('show:error', 'Error al crear cuenta debe aceptar las notificaciones');
      }
    }
  }

  public facebookRegister() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.auth.facebookLogin(res.authResponse.accessToken)
          .then(rsp => {
              if (rsp.success) {
                switch (rsp.role) {
                  case  'admin' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.nav.setRoot(HomePage);
                    break;
                  case 'customer' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.nav.setRoot(ClientHomePage);
                    break;
                  case 'provider' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.nav.setRoot(ProviderHomePage);
                    break;
                  default :
                    this.events.publish('hide:loading');
                    this.nav.setRoot(ClientHomePage);
                    break;
                }
              }
              else {
                this.events.publish('show:error', 'Error logging into Facebook');
              }
            },
            error => {
              console.log(error);
              this.events.publish('show:error', 'Error logging into Facebook');
            })
          .catch((err) => {
            console.log(err);
            this.events.publish('show:error', 'Error logging into Facebook');

          })
      })
      .catch(e => console.log('Error en autenticación de Facebook', e));
  }

  check() {
    this.events.publish('show:menu', 'customer');
    this.nav.setRoot(ClientHomePage);
  }

  @HostListener('keydown', ['$event']) onInputChange(event) {
    if (event.srcElement.tagName !== "INPUT") {
      return;
    }
    var code = event.keyCode || event.which;
    debugger;
    if (code === 13 || code === 9) {
      if (this.signupSlider.getActiveIndex() != 1) {
        //  this.keyboard.close();
        event.preventDefault();
        this.next();
      }
    }
  }

  openUrl(url) {
    this.iab.create(url);
  }
}
