import {Component, ViewChild, HostListener} from '@angular/core';
import {NavController, NavParams, Events, Platform} from 'ionic-angular';

import {ClientProvider} from '../../../providers/client/client';

import {ClientHomePage} from '../client-home/client-home';

import {AuthServiceProvider} from '../../../providers/auth-service/auth-service';

import {Keyboard} from '@ionic-native/keyboard';

declare var Card;
declare var Mercadopago;

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {UtilServiceProvider} from "../../../providers/util-service/util-service";


@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {
  public request: any = {};
  public docTypes = [];
  public typeNumDoc = "number";
  public cardNumber = "4242";
  public defaultDoc = "RUT";
  card: any;
  @ViewChild('cardSlider') cardSlider: any;
  @ViewChild('number') numberInput;
  @ViewChild('name') nameInput;
  @ViewChild('expiry') expiryInput;
  @ViewChild('cvc') cvcInput;
  //@ViewChild('docType') docType;
  @ViewChild('docNumberInput') docNumberInput;
  @ViewChild('emailInput') emailInput;
  public email = "";
  currentIndex = "";
  numberError = false;
  nameError = false;
  expiryError = false;
  cvcError = false;
  emailError = false;
  docError = false;
  docnumberError = false;
  isIos;
  yyList = [];
  iosForm: FormGroup;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private clientProvider: ClientProvider,
              private events: Events,
              public auth: AuthServiceProvider,
              private util: UtilServiceProvider,
              private keyboard: Keyboard,
              public formBuilder: FormBuilder,
              public platform: Platform) {

    this.request = this.navParams.get('request');
    auth.getUserInfo().then(user => {
      this.email = user.email
    });
    this.getYyList();

    this.iosForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      number: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
      cvc: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
      month: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required])],
      year: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required])]
    });

    this.isIos = this.platform.is('ios');

  }

  ionViewDidLoad() {
    this.setupMercadoPago();
    setTimeout(() => {
      this.numberInput.setFocus();
    }, 1000);

    this.cardSlider.onlyExternal = true;


  }

  setupMercadoPago() {

    // Mercadopago.setPublishableKey("APP_USR-30997425-998d-41f0-9ed9-91f66b04b5b8"); //RUT example 157578596 4168818844447115
    Mercadopago.setPublishableKey(this.util.public_key);
    //TEST-435fe83e-19e1-4f44-a629-b2a538b99fad
    //APP_USR-30997425-998d-41f0-9ed9-91f66b04b5b8
    if (!this.isIos) {
      try {
        new Card({
          form: document.querySelector('#form'),
          container: '.card-wrapper',
          placeholders: {
            number: '&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;',
            cvc: '&bull;&bull;&bull;',
            expiry: '&bull;&bull;/&bull;&bull;',
            name: 'NOMBRE APELLIDO'
          },
          messages: {
            validDate: 'fecha\nexp',
            monthYear: 'mm/yy'
          }
        });
      } catch (e) {
        console.log("##e: " + e.message);
      }
    }

    Mercadopago.getIdentificationTypes((status, docTypes) => {
      if (status != 200 && status != 201) {
        console.log("ERROR::MERCADOPAGO::DOCTYPES");
        this.events.publish('show:error', 'Error mercadoPago DocTypes, verifica tu conexión a la red');
        setTimeout(() => {
          this.navCtrl.pop();
        }, 3500);
      }
      else {
        //debugger;
        this.docTypes = docTypes;
      }
    });


  }

  doPay() {
    // debugger;
    let mpForm;
    if (this.isIos && !this.iosForm.valid) {
      this.submitAttempt = true;
      this.prev();
      this.events.publish('show:error', 'Por favor llene todos los campos obligatorios');
      return;
    }
    else {
      if (this.isIos) {
        mpForm = this.createMpForm(this.iosForm.value);
      }
      else {
        mpForm = this.createMpForm();
      }

      Mercadopago.clearSession();

      if (!mpForm) {
        this.events.publish('show:error', 'Por favor llene todos los campos obligatorios');
        return;
      }
      this.events.publish('show:loading');
      let ctx = this;
      Mercadopago.createToken(mpForm, (status, response) => {
        if (status != 200 && status != 201) {
          let errorMsg = "Error al realizar pago. Por favor revise los datos.";
          if (response.cause) {
            errorMsg = ctx.getErrorTokenMp(response.cause);
          }
          this.events.publish('show:error', errorMsg);
          return;
        }
        else {
          let token: any;
          token = response.id;
          ctx.request.car = ctx.request.car._id;
          ctx.request.token = response.id;
          ctx.request.email = this.email;
          ctx.request.payment_method_id = "visa";
          Mercadopago.getPaymentMethod({"bin": this.cardNumber}, (status, response) => {
            if (status == 200) {
              ctx.request.payment_method_id = response[0].id ? response[0].id : ctx.request.payment_method_id;
            }
            ctx.clientProvider.CreateRequest(ctx.request).then(rsp => {
              if (rsp.success) {
                document.getElementById('content-resumen').style.display = 'none';
                document.getElementById('header-finish').style.display = 'none';
                document.getElementById('content-finish').style.display = '';
                ctx.events.publish('hide:loading');
              }
              else {
                this.events.publish('show:error', 'Error al crear la solicitud');
              }
            })
              .catch(error => {
                console.error("Error al crear la solicitud");
                this.events.publish('show:error', 'Error al crear la solicitud');
              });
          });
        }
      });
    }
  }

  goToHome() {
    this.navCtrl.setRoot(ClientHomePage);
  }

  next() {
    if (this.isIos) {
      this.cardSlider.slideTo(4);
    }
    else {
      this.cardSlider.update();
      let currentIndex = this.cardSlider.getActiveIndex();
      console.log('Current index is', currentIndex);
      this.cardSlider.slideNext();
      switch (currentIndex) {
        case 0 :
          setTimeout(() => {
            this.nameInput.setFocus();
          }, 500);
          break;
        case 1 :
          setTimeout(() => {
            this.expiryInput.setFocus();
          }, 500);
          break;
        case 2 :
          setTimeout(() => {
            this.cvcInput.setFocus();
          }, 500);
          break;
        case 3 :
          setTimeout(() => {
            this.emailInput.setFocus();
          }, 500);

          break;
        case 4 :

          break;

      }
    }

  }

  prev() {
    if (this.isIos) {
      this.cardSlider.update();
      let currentIndex = this.cardSlider.getActiveIndex();
      if (currentIndex == 0) {
        this.navCtrl.pop();
      }
      else {
        this.cardSlider.slideTo(0);
      }
    }
    else {
      this.cardSlider.update();
      let currentIndex = this.cardSlider.getActiveIndex();
      console.log('Current index is', currentIndex);
      this.cardSlider.slidePrev();

      switch (currentIndex) {
        case 0 :
          this.navCtrl.pop();
          break;
        case 1 :
          setTimeout(() => {
            this.numberInput.setFocus();
          }, 500);
          break;
        case 2 :
          setTimeout(() => {
            this.nameInput.setFocus();
          }, 500);
          break;
        case 3 :
          setTimeout(() => {
            this.expiryInput.setFocus();
          }, 500);
          break;
        case 4 :
          setTimeout(() => {
            this.cvcInput.setFocus();
          }, 500);
          break;

      }
    }

  }

  createMpForm(formValues = undefined) { //Mercadopago form

    var form = document.createElement("form");

    if (!formValues) {
      formValues = {};
      //card number:
      if (this.numberInput.value == "") {
        this.numberError = true;
        this.cardSlider.slideTo(0);
        return false;
      }
      var cardNumber = "";
      this.numberInput.value.split(" ").forEach(function (element) {
        cardNumber += element;
      });
      formValues.number = cardNumber;
      //card name:
      if (this.nameInput.value == "") {
        this.nameError = true;
        this.cardSlider.slideTo(1);
        return false;
      }
      formValues.name = this.nameInput.value;
      //expiration date:
      if (this.expiryInput.value == "") {
        this.expiryError = true;
        this.cardSlider.slideTo(2);
        return false;
      }
      var monthYear = this.expiryInput.value;
      monthYear = (monthYear != undefined && monthYear.split("/").length > 1) ? monthYear.split("/") : ["", ""];
      formValues.month = monthYear[0].trim();
      formValues.year = monthYear[1].trim();
      //security code:
      if (this.cvcInput.value == "") {
        this.cvcError = true;
        this.cardSlider.slideTo(3);
        return false;
      }
      formValues.cvc = this.cvcInput.value;
    }

    this.cardNumber = formValues.number;

    var cardInput = document.createElement("input");
    cardInput.setAttribute("type", "text");
    cardInput.setAttribute("id", "cardNumber");
    cardInput.setAttribute("data-checkout", "cardNumber");
    cardInput.setAttribute("value", formValues.number);
    form.appendChild(cardInput);

    var nameInput = document.createElement("input");
    nameInput.setAttribute("type", "text");
    nameInput.setAttribute("id", "cardholderName");
    nameInput.setAttribute("data-checkout", "cardholderName");
    nameInput.setAttribute("value", formValues.name);
    form.appendChild(nameInput);

    var expirationMonthInput = document.createElement("input");
    expirationMonthInput.setAttribute("type", "text");
    expirationMonthInput.setAttribute("id", "cardExpirationMonth");
    expirationMonthInput.setAttribute("data-checkout", "cardExpirationMonth");
    expirationMonthInput.setAttribute("value", formValues.month);
    form.appendChild(expirationMonthInput);

    var expirationYearInput = document.createElement("input");
    expirationYearInput.setAttribute("type", "text");
    expirationYearInput.setAttribute("id", "cardExpirationYear");
    expirationYearInput.setAttribute("data-checkout", "cardExpirationYear");
    expirationYearInput.setAttribute("value", formValues.year);
    form.appendChild(expirationYearInput);

    var codeInput = document.createElement("input");
    codeInput.setAttribute("type", "text");
    codeInput.setAttribute("id", "securityCode");
    codeInput.setAttribute("data-checkout", "securityCode");
    codeInput.setAttribute("value", formValues.cvc);
    form.appendChild(codeInput);

    if (this.email == "") {
      this.emailError = true;
      return false;
    }
    var emailInput = document.createElement("input");
    emailInput.setAttribute("id", "email");
    emailInput.setAttribute("name", "email");
    emailInput.setAttribute("type", "email");
    emailInput.setAttribute("value", this.email);
    form.appendChild(emailInput);

    /*if(this.docType.value.length == 0){
      this.docError = true;
      return false;
    }*/
    var docTypeSelect = document.createElement("select");
    docTypeSelect.setAttribute("type", "text");
    docTypeSelect.setAttribute("id", "docType");
    docTypeSelect.setAttribute("data-checkout", "docType");
    var option = document.createElement("option");
    option.setAttribute("value", /*this.docType.value*/this.defaultDoc);
    option.setAttribute("selected", "true");
    docTypeSelect.add(option);
    form.appendChild(docTypeSelect);
    if (this.docNumberInput.value == "") {
      this.docnumberError = true;
      return false;
    }

    var docNumberInput = document.createElement("input");
    docNumberInput.setAttribute("type", "text");
    docNumberInput.setAttribute("id", "docNumber");
    docNumberInput.setAttribute("data-checkout", "docNumber");
    docNumberInput.setAttribute("value", this.docNumberInput.value);
    form.appendChild(docNumberInput);

    return form;
  }

  @HostListener('keydown', ['$event']) onInputChange(event) {
    if (event.srcElement.tagName !== "INPUT") {
      return;
    }
    var code = event.keyCode || event.which;
    if (code === 13 || code === 9) {
      if (this.cardSlider.getActiveIndex() != 4) {
        // this.keyboard.close();
        this.keyboard.hide();
        event.preventDefault();
        this.next();
      }
    }
  }

  onChangeDoc(evt) {
    //  console.log(evt);
  }


  getErrorTokenMp(errorsMp) { //doc ref https://www.mercadopago.com.ve/developers/es/solutions/payments/custom-checkout/response-handling/#card-token
    this.numberError = false;
    this.nameError = false;
    this.expiryError = false;
    this.cvcError = false;
    this.emailError = false;
    this.docError = false;
    this.docnumberError = false;
    errorsMp.map(error => {
      switch (error.code) {
        case "205" :
          this.numberError = true;
          break;
        case "208" :
          this.expiryError = true;
          break;
        case "209" :
          this.expiryError = true;
          break;
        case "212" :
          this.docError = true;
          break;
        case "213" :
          //TODO revisar este error
          break;
        case "214" :
          this.docnumberError = true;
          break;
        case "220" :
          //TODO revisar este error
          break;
        case "221" :
          this.nameError = true;
          break;
        case "224" :
          this.cvcError = true;
          break;
        case "E301" :
          this.numberError = true;
          break;
        case "E302" :
          this.cvcError = true;
          break;
        case "316" :
          this.nameError = true;
          break;
        case "322" :
          this.docError = true;
          break;
        case "323" :
          //TODO revisar este error
          break;
        case "324" :
          this.docnumberError = true;
          break;
        case "325" :
          this.expiryError = true;
          break;
        case "326" :
          this.expiryError = true;
          break;
      }
    });

    if (this.numberError) {
      this.cardSlider.slideTo(0);
      return "Error al realizar pago. Revise los datos de la tarjeta.";
    }
    if (this.nameError) {
      this.cardSlider.slideTo(1);
      return "Error al realizar pago. Revise su nombre.";
    }
    if (this.expiryError) {
      this.cardSlider.slideTo(2);
      return "Error al realizar pago. Revise la fecha de expiración.";
    }
    if (this.cvcError) {
      this.cardSlider.slideTo(3);
      return "Error al realizar pago. Revise el código de seguridad.";
    }
    if (this.docError) {
      return "Error al realizar pago. Revise el tipo de documento de identidd.";
    }
    if (this.docnumberError) {
      return "Error al realizar pago. Revise el documento de identidad.";
    }
    else {
      return "Error al realizar pago. Intente nuevamente.";
    }
  }

  getMarginTop() {
    if (this.isIos) {
      return "0px";
    }
    else {
      return "56px";
    }
  }

  getYyList() {
    this.yyList = [];
    let currentY: number = parseInt(new Date().getFullYear().toString().substr(2));
    for (var i = 0; i <= 10; i++) this.yyList.push(currentY + i);
  }


}
