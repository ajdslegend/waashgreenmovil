import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';

import {ModeSelectionPage} from '../mode-selection/mode-selection';
import {ClientProvider} from '../../../providers/client/client';
import {AlertController} from 'ionic-angular';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

@Component({
  selector: 'page-vehicle-selection',
  templateUrl: 'vehicle-selection.html',
})
export class VehicleSelectionPage {
  itemsCars = [];
  private api;

  constructor(
    public navCtrl: NavController,
    private events: Events,
    private clientProvider: ClientProvider,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public util: UtilServiceProvider
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

    this.getCarUser();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleSelectionPage');
  }

  /*  ionViewWillEnter() {
      //var ctx = this;
      this.events.publish('hide:loading');
      //setTimeout(function(){ctx.getdashboard()},0);
    }*/

  next(car) {
    let alert = this.alertCtrl.create({
      title: "Recordatorio",
      subTitle: "<ul><li>El auto debe estar sin barro.</li><li style='text-align: justify;'>El auto debe estar a la sombra.</li><li style='text-align: justify;'>No dejar objetos de valor en el auto.</li><li style='text-align: justify;'>Avisa que irán a lavar el auto.</li></ul>",
      buttons: ['Aceptar']
    });
    alert.present();

    this.navCtrl.push(ModeSelectionPage, {
      request: {car: car}
    });
  }


  getCarUser() {
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.clientProvider.getcars()

      .then(rsp => {
          if (rsp.success) {
            rsp.data.map(request => {
              request.image_url = (!request.image_url || request.image_url == "") ? "assets/img/mycar.png" : this.api + request.image_url;
              this.itemsCars.push(request)

            });
            this.events.publish('hide:loading');
            // this.currentDashboard = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }


  itemdelel(event, item) {
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.clientProvider.deletCar(item)

      .then(rsp => {
          if (rsp.success) {

            this.events.publish('hide:loading');
            this.events.publish('show:popup', rsp.data.response.message);

          }
          else {
            this.events.publish('hide:loading');
            this.events.publish('show:popup', 'Error al eliminar vehiculo, intente nuevamente');
          }
        },
        error => {
          this.events.publish('hide:loading');
          this.events.publish('show:popup', 'Error al  eliminar vehiculo, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', 'Error al  eliminar vehiculo, intente nuevamente');
      })

  }


}
