import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {ClientsinPage} from '../client-sin/client-sin';
import {historicalDetailPage} from '../historical-detail/historical-detail';
import {clientUpdatePage} from '../client-update/client-update';
import {clientCarPage} from '../Client-new-car/client-car';


import {VehicleSelectionPage} from '../vehicle-selection/vehicle-selection';
import {ClientProvider} from '../../../providers/client/client';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-client-home',
  templateUrl: 'client-home.html',
})
export class ClientHomePage {
  administrar;
  itemsPending = [];
  itemsFinish = [];
  statusColor: any;
  public api;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private clientProvider: ClientProvider,
    private events: Events,
    public util: UtilServiceProvider) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
  }

  ionViewWillEnter() {
    let ctx: any;
    ctx = this;
    this.administrar = "Pendiente";
    setTimeout(function () {
      ctx.getManagerService()
    }, 0);
  }

  logout() {
    this.events.publish('user:logout');
  }

  newRequest() {
    this.navCtrl.push(VehicleSelectionPage);
  }

  rediredClientCar(event, item) {
    this.navCtrl.push(clientCarPage, {
      item: item
    });
  }

  rediredhistoricalDetail(event, item) {
    this.navCtrl.push(historicalDetailPage, {
      item: item
    });
  }

  clientUpdateredired(event, item) {
    this.navCtrl.push(clientUpdatePage, {
      item: item
    });
  }

  clientSin(event, item) {
    this.navCtrl.push(ClientsinPage, {
      item: item
    });
  }

  getManagerService() {
    this.itemsPending = [];
    this.itemsFinish = [];
    this.events.publish('show:loading');
    this.clientProvider.getclientRequest()
      .then(rsp => {
          if (rsp.success) {
            rsp.data.map(request => {
              request.car_detail = !request.car_detail ? {type: "", model: {}} : request.car_detail;
              if (request.status !== undefined) {
                var listType;
                var listIdx;
                request.date_at = new Date(request.date_at);
                request.date = (request.date_at != "Invalid Date") ? request.date_at.getDate() + "/" + (request.date_at.getMonth() + 1) + "/" + request.date_at.getFullYear() : "";
                if (!request.address) request.address = {};
                if (request.status == "pending") {
                  request.statuse = "Pendiente";
                  this.itemsPending.push(request)
                  listType = "pending";
                  listIdx = this.itemsPending.length - 1;
                }
                else if (request.status == "in_progress") {
                  request.statuse = "En Progreso";
                  this.itemsPending.push(request);
                  listType = "pending";
                  listIdx = this.itemsPending.length - 1;
                }
                else if (request.status == "finish") {
                  this.itemsFinish.push(request);
                  listType = "finish";
                  listIdx = this.itemsFinish.length - 1;
                }
                else {
                  return;
                }

                if (request.take_by !== undefined) {
                  this.clientProvider.getproviderDetail(request.take_by, listType, listIdx).then((rsp) => {
                      if (rsp.success && rsp.data != null) {
                        if (rsp.listType == "pending") {
                          this.itemsPending[rsp.listIdx].take_by = rsp.data;
                          this.itemsPending[rsp.listIdx].provedorname = rsp.data.name + " " + rsp.data.last_name;

                        }
                        else {
                          this.itemsFinish[rsp.listIdx].take_by = rsp.data;
                          this.itemsFinish[rsp.listIdx].provedorname = rsp.data.name + " " + rsp.data.last_name;

                        }
                      }
                      else {
                        console.log("error al cargar detalle de proveedor");
                      }
                    },
                    error => {
                      console.log("error al cargar detalle de proveedor");
                    })
                    .catch((err) => {
                      console.log("error al cargar detalle de proveedor");
                    });
                } else {
                  request.provedorname = "por asignar";
                }
              }
            });
            var ctx = this;
            setTimeout(function () {
              ctx.events.publish('hide:loading')
            }, 1500);
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
            let ctx = this;
            setTimeout(function () {
              ctx.events.publish('hide:loading')
            }, 1500);
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          let ctx = this;
          setTimeout(function () {
            ctx.events.publish('hide:loading')
          }, 1500);
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        let ctx = this;
        setTimeout(function () {
          ctx.events.publish('hide:loading')
        }, 1500);
      })
  }

  refresh() {
    this.events.publish('openPage', 'home');
  }

}
