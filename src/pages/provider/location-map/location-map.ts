import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  MarkerOptions,
  LatLng
} from '@ionic-native/google-maps';


@Component({
  selector: 'page-location-map',
  templateUrl: 'location-map.html',
})
export class LocationMapPage {
  locInfo: any;
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  markerOptions: MarkerOptions;
  latlng = {lat: 0.0, lng: 0.0};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public googleMaps: GoogleMaps,
              public events: Events) {
    this.locInfo = navParams.get('locInfo');
    //  this.events.publish('show:loading');
  }

  ionViewDidLoad() {
    this.loadMap({lat: this.locInfo.coordinates[0], lng: this.locInfo.coordinates[1]});
  }

  loadMap(target) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: target,
        zoom: 17,
        tilt: 27
      },
      controls: {
        zoom: true
      }
    };
    this.map = GoogleMaps.create(this.mapElement.nativeElement, mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        let markerOptions: MarkerOptions = {
          position: target,
          title: "Dirección para el lavado"
        };
        this.addMarker(markerOptions);
        this.events.publish('hide:loading');
      });
  }

  addMarker(options) {
    let markerOptions: MarkerOptions = {
      position: new LatLng(options.position.lat, options.position.lng),
      title: options.title,
    };
    this.map.addMarker(markerOptions);
    this.latlng.lat = options.position.lat;
    this.latlng.lng = options.position.lng;
    this.map.setCameraTarget(new LatLng(options.position.lat, options.position.lng));
    this.map.setMyLocationEnabled(true);
    this.map.setCompassEnabled(true);

  }

}
