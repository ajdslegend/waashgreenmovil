import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  MarkerOptions,
  Marker,
  LatLng
} from '@ionic-native/google-maps';
import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {FormBuilder} from '@angular/forms';
import {LocationMapPage} from '../location-map/location-map';
import {providerProvider} from '../../../providers/provider/provider';
import {ProviderHomePage} from '../provider-home/provider-home';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-service-detail',
  templateUrl: 'detail-Service.html',
})
export class serviceDetailPage {
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  myMarker: Marker;
  markerOptions: MarkerOptions;
  latlng = {lat: 0.0, lng: 0.0};
  tipo = "none";
  selectedItem: any;
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public events: Events,
              private ProviderProvider: providerProvider,
              public googleMaps: GoogleMaps,
              public util: UtilServiceProvider) {
    //this.events.publish('show:loading');
    this.selectedItem = navParams.get('item');
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.showType(this.selectedItem.type);
    if (!this.selectedItem.car_detail) this.selectedItem.car_detail = {type: "", model: {car_brand: {}}}
    if (!this.selectedItem.car_detail.model.car_brand) this.selectedItem.car_detail.model.car_brand = {};

  }

  ionViewDidLoad() {
    this.loadMap({lat: this.selectedItem.loc.coordinates[0], lng: this.selectedItem.loc.coordinates[1]});
  }

  showType(type) {
    debugger
    switch (type) {
      case "in_and_out":
        this.tipo = "Lavado Exterior e Interior";
        break;
      case "ext_only":
        this.tipo = "Lavado Exterior";
        break;
      default:
        this.tipo = "Por Asignar";
        break;
    }
    debugger
  }

  loadMap(target) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: target,
        zoom: 16,
        tilt: 27
      },
      controls: {
        zoom: false
      }
    };
    this.map = GoogleMaps.create(this.mapElement.nativeElement, mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        let markerOptions: MarkerOptions = {
          position: target,
          title: "Dirección para el lavado"
        };
        this.addMarker(markerOptions);
        this.events.publish('hide:loading');
      });
  }

  addMarker(options) {
    let markerOptions: MarkerOptions = {
      position: new LatLng(options.position.lat, options.position.lng),
      title: options.title,
    };
    this.map.addMarker(markerOptions);
    this.latlng.lat = options.position.lat;
    this.latlng.lng = options.position.lng;
    this.map.setCameraTarget(new LatLng(options.position.lat, options.position.lng));
    this.map.setAllGesturesEnabled(false);
    this.map.setCompassEnabled(false);

  }

  showLoc() {
    this.navCtrl.push(LocationMapPage, {
      locInfo: {coordinates: this.selectedItem.loc.coordinates, address: this.selectedItem.address.name}
    });
  }


  take(id) {
    debugger
    // this.status = this.showStatus(status);
    this.events.publish('show:loading');
    this.ProviderProvider.takeRequest(id)

      .then(rsp => {
          if (rsp.success) {

            let ctx = this;
            setTimeout(function () {
              ctx.events.publish('hide:loading')
            }, 1500);
            this.redired(this.selectedItem);
          }
          else {
            this.events.publish('show:popup', 'Error al tomar solicitud, intente nuevamente');
          }
          this.events.publish('hide:loading');
        },
        error => {
          this.events.publish('hide:loading');
          if (error.id == "INSUFFICIENT_SUPPLEMENTS") {
            this.events.publish('show:popup', error.description);
          }
          else {
            this.events.publish('show:popup', 'Error al tomar solicitud, intente nuevamente');
          }

        })
      .catch((err) => {
        this.events.publish('hide:loading');
        this.events.publish('show:popup', 'Error al tomar solicitud, intente nuevamente');
      })
  }

  redired(item) {
    this.navCtrl.setRoot(ProviderHomePage, {
      item: item
    });
  }

}
