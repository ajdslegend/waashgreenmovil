import {Component} from '@angular/core';
import {NavController, NavParams, Events, ActionSheetController} from 'ionic-angular';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

import {providerProvider} from '../../../providers/provider/provider';
import {ProviderHomePage} from '../provider-home/provider-home';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

// import {Camera, CameraOptions} from '@ionic-native/camera';
import {Camera} from '@ionic-native/camera';
import {ImageResizer} from '@ionic-native/image-resizer';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'page-complete-request',
  templateUrl: 'complete-request.html',
})
export class clientCompleteRequestPage {
  FormComplete: FormGroup;
  submitAttempt: boolean = false;
  isImage = false;
  qtybolean: boolean = false;
  selectedItem: any;
  public Image_Finish = "assets/img/carg.png";
  itemsCarsbrands = [];
  itemscarmodels = [];
  //api = 'https://waashgreenapi.herokuapp.com/api';
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private events: Events,
              private ProviderProvider: providerProvider,
              public util: UtilServiceProvider,
              private camera: Camera,
              public actionSheetCtrl: ActionSheetController,
              public imageResizer: ImageResizer,
              public _DomSanitizationService: DomSanitizer
  ) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });
    this.selectedItem = navParams.get('item');
    this.FormComplete = formBuilder.group({
      observations: ['', Validators.compose([Validators.required])],
    });
  }

  complete() {

    if (!this.FormComplete.valid) {
      this.submitAttempt = true;
      if (this.Image_Finish == "assets/img/carg.png") {
        this.isImage = true;
      }
    } else {
      if (this.Image_Finish == "assets/img/carg.png") {
        this.isImage = true;
      } else {
        this.events.publish('show:loading');
        var options = {
          params: {
            name: 'finish',
            rut: this.selectedItem._id
          }
        };

        this.util.uploadFile(this.Image_Finish, options)
          .then(imageUrl => {
            return this.ProviderProvider.completeRequest(this.selectedItem._id, imageUrl.data, this.FormComplete.value.observations);
          })
          .then(rsp => {
              this.events.publish('hide:loading');

              if (rsp.success) {
                this.events.publish('show:popup', 'Solicitud completada de forma exitosa');
                this.navCtrl.setRoot(ProviderHomePage);
              } else {
                this.events.publish('show:popup', 'Error al completar Solicitud, intente nuevamente');
              }
            },
            error => {
              this.events.publish('hide:loading');

              this.events.publish('show:popup', 'Error al completar Solicitud, intente nuevamente');
            })
          .catch((err) => {
            this.events.publish('hide:loading');

            this.events.publish('show:popup', 'Error al completar Solicitud, intente nuevamente');
          })
      }
    }
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Selecciona una imagen',
      buttons: [
        {
          text: 'Galería',
          handler: () => {
            this.openGallery();
          }
        },
        {
          text: 'Cámara',
          handler: () => {
            this.opencamera();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  openGallery() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      quality: 40,
      targetWidth: 1280,
      targetHeight: 1280
    }).then((imageData) => {
      this.Image_Finish = 'data:image/jpeg;base64,' + imageData;
      console.log("image: " + this.Image_Finish)
      this.isImage = false;
    }).catch((error) => {
      console.log(error)
    })
  }

  opencamera() {
    try {
      this.camera.getPicture({
        quality: 40,
        destinationType: this.camera.DestinationType.FILE_URI,
        targetWidth: 1280,
        targetHeight: 1280,
        allowEdit: true,
        encodingType: this.camera.EncodingType.JPEG
      }).then((uri) => {
        this.Image_Finish = uri;
        this.isImage = false;
      }).catch((error) => {
        console.warn(error)
      })

      // let options: CameraOptions = {
      //   quality: 100,
      //   destinationType: this.camera.DestinationType.DATA_URL,
      //   targetWidth: 700,
      //   targetHeight: 700,
      //   allowEdit: true,
      //   mediaType: this.camera.MediaType.PICTURE,
      //   encodingType: this.camera.EncodingType.JPEG
      // };
      //
      // this.camera.getPicture(options).then((imageData) => {
      //   this.Image_Finish = 'data:image/jpeg;base64,' + imageData;
      //   console.log("||image: " + this.Image_Finish)
      //   this.isImage = false;
      // }).catch((error) => {
      //   console.log(error)
      // })

    } catch (e) {
      console.log("#e camera: " + e.message)
    }

  }


}
