import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  MarkerOptions,
  Marker,
  LatLng
} from '@ionic-native/google-maps';
import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, NavParams, Events} from 'ionic-angular';
import {FormBuilder} from '@angular/forms';
import {LocationMapPage} from '../location-map/location-map';
import {providerProvider} from '../../../providers/provider/provider';
import {clientCancelRequestPage} from '../cancel-request/cancel-request';
import {clientCompleteRequestPage} from '../complete-request/complete-request';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-detail-Service-inProgress',
  templateUrl: 'detail-Service-inProgress.html',
})
export class serviceDetailinProgressPage {
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  myMarker: Marker;
  markerOptions: MarkerOptions;
  latlng = {lat: 0.0, lng: 0.0};
  tipo = "none";
  selectedItem: any = {};
  private api;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public events: Events,
              public ProviderProvider: providerProvider,
              public googleMaps: GoogleMaps,
              public util: UtilServiceProvider) {
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

    this.selectedItem = navParams.get('item');
    this.showType(this.selectedItem.type);
    if (!this.selectedItem.car_detail) this.selectedItem.car_detail = {type: "", model: {car_brand: {}}}
  }

  ionViewDidLoad() {
    this.loadMap({lat: this.selectedItem.loc.coordinates[0], lng: this.selectedItem.loc.coordinates[1]});
  }

  showType(type) {
    switch (type) {
      case "in_and_out":
        this.tipo = "Lavado Exterior e Interior";
        break;
      case "ext_only":
        this.tipo = "Lavado Exterior";
        break;
      default:
        this.tipo = "Por Asignar";
        break;
    }
  }

  loadMap(target) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: target,
        zoom: 16,
        tilt: 27
      },
      controls: {
        zoom: false
      }
    };
    this.map = GoogleMaps.create(this.mapElement.nativeElement, mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        let markerOptions: MarkerOptions = {
          position: target,
          title: "Dirección para el lavado"
        };
        this.addMarker(markerOptions);
        this.events.publish('hide:loading');
      });
  }

  addMarker(options) {
    let markerOptions: MarkerOptions = {
      position: new LatLng(options.position.lat, options.position.lng),
      title: options.title,
    };
    this.map.addMarker(markerOptions);
    this.latlng.lat = options.position.lat;
    this.latlng.lng = options.position.lng;
    this.map.setCameraTarget(new LatLng(options.position.lat, options.position.lng));
    this.map.setAllGesturesEnabled(false);
    this.map.setCompassEnabled(false);

  }

  showLoc() {
    this.navCtrl.push(LocationMapPage, {
      locInfo: {coordinates: this.selectedItem.loc.coordinates, address: this.selectedItem.loc.address}
    });
  }

  redired(item) {
    this.navCtrl.push(clientCancelRequestPage, {
      item: item
    });
  }

  redired2(item) {
    this.navCtrl.push(clientCompleteRequestPage, {
      item: item
    });
  }

}
