import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {serviceDetailPage} from '../detail-Service/detail-Service';
import {providerProvider} from '../../../providers/provider/provider';
import {ClientProvider} from '../../../providers/client/client';
import {LocationMapPage} from '../location-map/location-map';
import {serviceDetailinProgressPage} from '../detail-Service-inProgress/detail-Service-inProgress';
import {AuthServiceProvider} from '../../../providers/auth-service/auth-service';

import {UtilServiceProvider} from '../../../providers/util-service/util-service';


import moment from 'moment';

@Component({
  selector: 'page-provider-home',
  templateUrl: 'provider-home.html',
})
export class ProviderHomePage {
  administrar;
  itemsPending = [];
  itemsScheduled = [];
  itemsFinish = [];
  statusColor: any;
  validador: false;
  public api;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private ProviderProvider: providerProvider,
    public clientProvider: ClientProvider,
    private events: Events,
    private auth: AuthServiceProvider,
    public util: UtilServiceProvider) {
    this.administrar = "Reciente";
    util.getBaseUrl().subscribe((base_url) => {
      this.api = base_url;
    });

  }

  ionViewWillEnter() {
    let ctx = this;
    this.administrar = "Reciente";
    this.itemsPending = [];
    this.itemsScheduled = [];
    this.itemsFinish = [];
    setTimeout(function () {
      ctx.getManagerServiceProgress()
    }, 0);
  }

  logout() {
    this.events.publish('user:logout');
  }


  ver(event, item) {
    this.navCtrl.push(serviceDetailPage, {
      item: item
    });
  }


  getManagerServicePending() {

    // this.status = this.showStatus(status);
    //this.events.publish('show:loading');
    let currentProvider = "";
    this.auth.getUserInfo()
      .then((user) => {
        // debugger;
        currentProvider = user._id;
        return this.ProviderProvider.getRequestsPending();
      })
      .then(rsp => {
          if (rsp.success) {

            // debugger;

            rsp.data.map(request => {

              console.log(request);

              if (request.status !== undefined) {
                // debugger
                request.date_at = new Date(request.date_at);
                request.date = (request.date_at != "Invalid Date") ? request.date_at.getDate() + "/" + (request.date_at.getMonth() + 1) + "/" + request.date_at.getFullYear() : "";
                request.hours = request.date_at.getHours();
                request.minutes = request.date_at.getMinutes();
                request.is_scheduled = request.is_scheduled;
                if (!request.address) request.address = {};
                if (!request.provider) {
                  request.car_detail = !request.car_detail ? {type: "", model: {}} : request.car_detail;
                  request.car_detail.model = !request.car_detail.model.name ? {} : request.car_detail.model;
                  this.itemsPending.push(request)

                } else {
                  if (request.provider._id == currentProvider && request.is_scheduled) {
                    // debugger;
                    request.car_detail = !request.car_detail ? {type: "", model: {}} : request.car_detail;
                    request.car_detail.model = !request.car_detail.model.name ? {} : request.car_detail.model;
                    this.itemsScheduled.push(request)
                  }
                }
              }
            });

            this.itemsPending = this.itemsPending.map(request => {
              request.car_detail = !request.car_detail ? {type: ""} : request.car_detail;
              var requestDate = moment(new Date(request.updated_at));
              var currentDate = moment();
              var diff = currentDate.diff(requestDate, 'h');
              if (diff > 24) {
                request.updated_show = currentDate.diff(requestDate, 'd') + "d";
              }
              else {
                request.updated_show = diff + "h";
              }
              return request;
            });
            //setTimeout(function(){ctx.events.publish('hide:loading')},1500);
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }

  roundAmount(amount: any) {
    let amounts = parseFloat(amount).toFixed(2);
    return amounts;
  }


  getManagerServiceFinish() {

    this.ProviderProvider.getRequestsRecord("finish")

      .then(rsp => {
          if (rsp.success) {

            rsp.data.map(request => {

              if (request.status !== undefined) {
                request.date_at = new Date(request.date_at);
                request.date = (request.date_at != "Invalid Date") ? request.date_at.getDate() + "/" + (request.date_at.getMonth() + 1) + "/" + request.date_at.getFullYear() : "";
                request.hours = request.date_at.getHours();
                request.minutes = request.date_at.getMinutes();
                request.is_scheduled = request.is_scheduled;
                request.statuse = "Finalizado";
                if (!request.address) request.address = {};
                request.car_detail = !request.car_detail ? {type: "", model: {}} : request.car_detail;
                request.car_detail.model = !request.car_detail.model.name ? {} : request.car_detail.model;

                // try{
                //   request.coupons = request.coupons;
                // }catch (e1) {
                //   request.coupons = {};
                // }

                var discount;
                try {
                  discount = request.coupons.campaign_id.detail.discount;
                } catch (e) {
                  discount = 0;
                }

                request.discount = discount;

                this.itemsFinish.push(request)

              }
            });
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
        },
        error => {
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }


  getManagerServiceProgress() {
    // debugger;
    this.events.publish('show:loading');
    this.ProviderProvider.getRequestsRecord("in_progress")

      .then(rsp => {
          // debugger;
          if (rsp.success) {
            if (rsp.data.length > 0 && !!rsp.data[0]) { //hay solicitudes en progreso, se tomara la primera
              let request = rsp.data[0];
              request.date_at = new Date(request.date_at);
              request.date = (request.date_at != "Invalid Date") ? request.date_at.getDate() + "/" + (request.date_at.getMonth() + 1) + "/" + request.date_at.getFullYear() : "";
              request.hours = request.date_at.getHours();
              request.minutes = request.date_at.getMinutes();
              request.is_scheduled = request.is_scheduled;
              if (!request.address) request.address = {};
              this.redired(request);
            }
            else { //no hay solicitudes en progreso
              this.getManagerServiceFinish();
              this.getManagerServicePending();
            }
          }
          else {
            this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
          }
          let ctx = this;
          setTimeout(function () {
            ctx.events.publish('hide:loading')
          }, 1500);
        },
        error => {
          let ctx = this;
          setTimeout(function () {
            ctx.events.publish('hide:loading')
          }, 1500);
          this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
        })
      .catch((err) => {
        let ctx = this;
        setTimeout(function () {
          ctx.events.publish('hide:loading')
        }, 1500);
        this.events.publish('show:popup', 'Error al cargar solicitudes, intente nuevamente');
      })
  }

  showLoc(item) {
    this.navCtrl.push(LocationMapPage, {
      locInfo: {coordinates: item.loc.coordinates, address: item.address.name}
    });

  }


  redired(item) {
    this.navCtrl.setRoot(serviceDetailinProgressPage,
      {
        item: item
      });
  }

  refresh() {
    this.events.publish('openPage', 'home-provider');
  }


}
