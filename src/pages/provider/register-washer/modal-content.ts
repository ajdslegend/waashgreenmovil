import {Component} from '@angular/core';
import {Platform, NavParams, ViewController} from 'ionic-angular';
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Events} from 'ionic-angular';


@Component({
  selector: 'page-modal-adress',
  templateUrl: 'modal-address.html'
})
export class ModalContentPage {
  result: any = [];
  option;
  selectedRegionItem;
  selectedProvinceItem;
  addressForm: FormGroup;
  communesForm: FormGroup;
  regions: any = [];
  provinces: any = [];
  communes: any = [];

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    private util: UtilServiceProvider,
    public formBuilder: FormBuilder,
    public events: Events
  ) {

    this.option = this.params.get('option');

    if (this.option == 1) {

      this.addressForm = formBuilder.group({
        region: ['', Validators.compose([Validators.required])],
        province: ['', Validators.compose([Validators.required])],
        commune: ['', Validators.compose([Validators.required])],
        street: ['', Validators.compose([Validators.maxLength(25), Validators.required])],
        department: ['', Validators.compose([Validators.maxLength(25), Validators.required])]
      });
    }
    else {
      this.result = this.params.get('result');
      if (this.result.length == 0) {
        this.util.resetRegions();
      }
      this.communesForm = formBuilder.group({
        region: ['', Validators.compose([Validators.required])],
        province: ['', Validators.compose([Validators.required])],
        commune: ['', Validators.compose([Validators.required])],
      });
    }

    this.getRegions();


  }

  getRegions() {
    this.events.publish('show:loading');
    this.util.getRegions()
      .then(rsp => {
          this.events.publish('hide:loading');
          if (rsp.success) {
            debugger;
            if (this.option == 1) {
              this.regions = rsp.data;
            }
            else {
              this.regions = rsp.data.filter(region => {
                if (region.status == "active") {
                  var provinces = region.provinces.filter(province => {
                    if (province.status == "active") {
                      var communes = province.communes.filter(commune => {
                        return commune.status == "active";
                      })
                      if (communes.length > 0) {
                        province.communes = communes;
                      }
                      else {
                        province.status = "inactive";
                      }
                    }
                    return province.status == "active";
                  });
                  if (provinces.length > 0) {
                    region.provinces = provinces;
                  }
                  else {
                    region.status = "inactive";
                  }
                }
                return region.status == "active";
              });


            }
          }
          else {
            console.log("Error al cargar regions.");
          }
        },
        error => {
          this.events.publish('hide:loading');

          console.log("Error al cargar regions.");
        })
      .catch((err) => {
        this.events.publish('hide:loading');

        console.log(err);
      })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onChange(newValue) {
    if (newValue != undefined && newValue != "") {
      this.selectedRegionItem = newValue;
      this.provinces = this.regions[newValue].provinces;
      this.communes = [];
      this.selectedProvinceItem = "";
    }
  }

  onChangeProvince(newValue) {
    if (newValue != undefined && newValue != "") {
      this.selectedProvinceItem = newValue;
      this.communes = this.provinces[newValue].communes;
    }
  }

  registerAddress() {
    if (this.addressForm.valid) {
      let address = this.addressForm.value.street + ", " + this.addressForm.value.department + ", " + this.addressForm.value.commune + ", " + this.provinces[this.addressForm.value.province].name + ", " + this.regions[this.addressForm.value.region].name;
      this.events.publish('modal:address', address);
      this.dismiss();
    }
  }

  updateCommunne(idx) {
    if (!this.communes[idx].select)
      this.communes[idx].select = true;
    else {
      this.communes[idx].select = false;
    }
  }

  addCommune() {
    let select = this.communes.filter((commune, idx) => {
      if (!this.regions[this.selectedRegionItem].provinces[this.selectedProvinceItem].communes[idx].selected) {
        this.regions[this.selectedRegionItem].provinces[this.selectedProvinceItem].communes[idx].selected = !!commune.select;
        return !!commune.select;
      }
      else {
        return false;
      }
    });


    this.result = this.result.concat(select);

    this.communes = [];
    this.provinces = [];
    this.selectedRegionItem = "";

  }

  registerCommunes() {
    this.events.publish('modal:communes', this.result);
    this.dismiss();
  }

  clear() {
    this.result = [];
    this.regions = [];
    this.provinces = [];
    this.communes = [];
    this.util.resetRegions();
    this.getRegions();
  }


}
