import {Component, ViewChild, ElementRef/*, Renderer*/, HostListener} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  ModalController,
  NavController,
  Events,
  ActionSheetController,
  Platform,
  ToastController,
  LoadingController,
  Loading
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";
import {UtilServiceProvider} from '../../../providers/util-service/util-service';

import {ModalContentPage} from './modal-content';
import {LoginPage} from '../../shared/login/login';
import {AlertController} from 'ionic-angular';

import {Camera} from "@ionic-native/camera";
/*import {File} from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileChooser } from '@ionic-native/file-chooser';*/

import {Keyboard} from '@ionic-native/keyboard';

import {CompleteService} from '../../../providers/complete-service/complete-service';

import {DomSanitizer} from '@angular/platform-browser';

import {AutoCompleteComponent} from 'ionic2-auto-complete';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {ImageResizer} from '@ionic-native/image-resizer';

//import { Base64 } from '@ionic-native/base64';


@Component({
  selector: 'page-registerWasher',
  templateUrl: 'register-washer.html',
})
export class RegisterWasherPage {
  Image_avatar: any = 'assets/img/img-empty.JPG';
  Image_cv: any = '';
  Image_ci_anv: any = 'assets/img/img-empty.JPG';
  Image_ci_rev: any = 'assets/img/img-empty.JPG';
  Image_ci_ant: any = '';
  @ViewChild('signupSlider') signupSlider: any;
  lastImage: string = null;
  loading: Loading;

  address = '';
  communes = {names: '', ids: []};

  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;

  submitAttempt: boolean = false;
  submitAttempt2: boolean = false;
  submitAttempt3: boolean = false;

  countries: any = [];
  vartest: any = "";
  selectCountry: boolean = true;

  public comResult = [];
  public fileTitle1 = "";
  public fileTitle2 = "";

  @ViewChild("input3")
  private nativeInputBtn: ElementRef;

  @ViewChild("input4")
  private nativeInputBtn2: ElementRef;


  @ViewChild('searchbar')
  searchbar: AutoCompleteComponent;


  constructor(
    public nav: NavController,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    private auth: AuthServiceProvider,
    private util: UtilServiceProvider,
    public events: Events,
    public modalCtrl: ModalController,
    /* private file: File,*/
    /*private renderer: Renderer,
    private filePath: FilePath,
    private fileChooser: FileChooser,
    private base64: Base64,*/
    public completeService: CompleteService,
    public keyboard: Keyboard,
    private iab: InAppBrowser,
    public alertCtrl: AlertController,
    public imageResizer: ImageResizer,
    public _DomSanitizationService: DomSanitizer
  ) {

    this.slideOneForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      last_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), Validators.required])],
      phone: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
      birth: ['', Validators.compose([Validators.required])],
      country: [''],
      agree: [false, Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'), Validators.required])],
      rut: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(8), Validators.pattern('[0-9]*'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirm_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      start: ['', Validators.compose([Validators.required])],
      end: ['', Validators.compose([Validators.required])],
      motivation: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      communes: ['', Validators.compose([Validators.required])],
      rut2: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(1), Validators.pattern('[a-zA-Z0-9]*'), Validators.required])]
    }, {validator: this.matchingPasswords('password', 'confirm_password')});

    this.util.getCountries()
      .then(rsp => {
          if (rsp.success) {
            this.countries = rsp.data;
          }
          else {
            this.events.publish('show:popup', 'Error', 'Error al cargar countries.');
          }
        },
        error => {
          this.events.publish('show:error', !error.description ? 'Error al cargar countries' : error.description);
        })
      .catch((err) => {
        console.log(err);
        this.events.publish('show:error', 'Error al cargar countries');
      });

    this.events.subscribe('modal:address', (address) => {
      this.slideOneForm.value.address = address;
      this.address = address;
    });

    this.events.subscribe('modal:communes', (communes) => {
      this.comResult = communes;
      this.communes = {names: '', ids: []};
      communes.map((commune) => {
        this.communes.names += commune.name + ", ";
        this.communes.ids.push(commune._id)
      });
      if (this.communes.names.length > 2) {
        this.communes.names = this.communes.names.substr(0, this.communes.names.length - 2);
      }
    });

  }

  ionViewDidLoad() {
    this.signupSlider.onlyExternal = true;
    this.slideOneForm.reset();
    this.util.resetRegions();

  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  next() {
    this.signupSlider.update();
    let currentIndex = this.signupSlider.getActiveIndex();
    console.log('Current index is', currentIndex);
    this.signupSlider.slideNext();

    if (currentIndex == 1) {
      let alert = this.alertCtrl.create({
        title: "Archivos",
        subTitle: "Los archivos deben ser máximo de 10Mb",
        buttons: ['Aceptar']
      });
      alert.present();
    }

  }

  prev() {
    this.signupSlider.update();
    let currentIndex = this.signupSlider.getActiveIndex();
    console.log('Current index is', currentIndex);
    this.signupSlider.slidePrev();
  }

  register() {
    if (!!this.searchbar.getSelection()) {

      this.slideOneForm.value.country = this.searchbar.getSelection()._id;

      if (!this.slideOneForm.valid) {
        if (this.slideOneForm.get('name').errors || this.slideOneForm.get('last_name').errors ||
          this.slideOneForm.get('phone').errors || this.slideOneForm.get('email').errors ||
          this.slideOneForm.get('birth').errors || this.slideOneForm.get('country').errors ||
          this.slideOneForm.get('address').errors) {
          //this.submitAttempt2 = false;
          this.submitAttempt = true;
          this.signupSlider.slideTo(0);
        }
        else if (this.slideOneForm.get('rut').errors || this.slideOneForm.get('rut2').errors ||
          this.slideOneForm.get('password').errors || this.slideOneForm.get('confirm_password').errors ||
          this.slideOneForm.get('start').errors || this.slideOneForm.get('end').errors ||
          this.slideOneForm.get('motivation').errors || this.slideOneForm.get('communes').errors) {
          //this.submitAttempt = false;
          this.submitAttempt2 = true;
          this.signupSlider.slideTo(1);
        }
      }
      else if (this.slideOneForm.value.agree) {

        if (this.Image_avatar == 'assets/img/img-empty.JPG' || this.Image_ci_anv == 'assets/img/img-empty.JPG' || this.Image_ci_rev == 'assets/img/img-empty.JPG') {
          this.events.publish('show:error', 'Error al crear cuenta, debe subir todos los archivos solicitados');
          this.submitAttempt3 = true;
        }
        else {

          this.submitAttempt3 = this.submitAttempt2 = this.submitAttempt3 = false;
          this.events.publish('show:loading');

          this.slideOneForm.value.rut = this.slideOneForm.value.rut + "-" + this.slideOneForm.value.rut2;

          var options = {
            params: {
              name: '',
              rut: this.slideOneForm.value.rut
            }
          };

          options.params.name = "avatar";

          debugger;
          this.util.uploadFile(this.Image_avatar, options)
            .then(avatarUrl => {
              this.slideOneForm.value.avatar_url = avatarUrl.data;
              options.params.name = "cv";
              return this.util.uploadFormFile(this.Image_cv, options);
            })
            .then(cvUrl => {
              this.slideOneForm.value.cv_url = cvUrl.data;
              options.params.name = "ci_anv";
              return this.util.uploadFile(this.Image_ci_anv, options);
            })
            .then(ciAnvUrl => {
              this.slideOneForm.value.ci_anv_url = ciAnvUrl.data;
              options.params.name = "ci_rev";
              return this.util.uploadFile(this.Image_ci_rev, options);
            })
            .then(ciRevUrl => {
              this.slideOneForm.value.ci_rev_url = ciRevUrl.data;
              options.params.name = "background";
              return this.util.uploadFormFile(this.Image_ci_ant, options);
            })
            .then(backgroundUrl => {
                this.slideOneForm.value.background_url = backgroundUrl.data;
                var birth = this.slideOneForm.value.birth.split("-");
                this.slideOneForm.value.birth = birth[1] + "/" + birth[2] + "/" + birth[0];
                this.slideOneForm.value.role = "provider";
                this.slideOneForm.value.schedule = [this.slideOneForm.value.start, this.slideOneForm.value.end];
                this.slideOneForm.value.communes = this.communes.ids;

                this.auth.register(this.slideOneForm.value)
                  .then(rsp => {
                      if (rsp.success) {
                        this.events.publish('hide:loading');
                        document.getElementById('content-sign-up').style.display = 'none';
                        document.getElementById('footer').style.display = 'none';
                        document.getElementById('header').style.display = 'none';
                        document.getElementById('content-check').style.display = '';
                        this.slideOneForm.reset();
                      }
                      else {
                        this.events.publish('show:popup', 'Error al crear cuenta, intente nuevamente');
                      }
                    },
                    error => {
                      this.events.publish('show:error', !error.description ? 'Error al crear cuenta' : error.description);
                    })
                  .catch((err) => {
                    console.log(err);
                    this.events.publish('show:error', !err.description ? 'Error al crear cuenta' : err.description);
                  })
              },
              error => {
                console.log(error);
                this.events.publish('show:error', !error.description ? 'Error al crear cuenta' : error.description);
                //this.events.publish('show:error',!error.description ? 'Error al crear cuenta' : error.description);
              })
            .catch((err) => {
              console.log(err);
              this.events.publish('show:error', !err.description ? 'Error al crear cuenta' : err.description);
              //this.events.publish('show:error','Error al crear cuenta');
            })
        }
      }
      else {
        this.events.publish('show:error', 'Error al crear cuenta, debe aceptar los términos y condiciones');
      }

    }
    else {
      this.selectCountry = false;
      this.signupSlider.slideTo(0);
    }

  }

  openModal(op) {
    if (op.option == 2) {
      op.result = this.comResult;
    }
    let modal = this.modalCtrl.create(ModalContentPage, op);
    modal.present();
  }


  accessGallery(id) {
    if (id != 2 && id != 5) {
      this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL, /*this.camera.DestinationType.DATA_URL*/
        allowEdit: true,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        quality: 40,
        targetWidth: 1280,
        targetHeight: 1280,
      }).then((imageData) => {
        switch (id) {
          case 1:
            this.Image_avatar = 'data:image/jpeg;base64,' + imageData;
            break;
          /* case 2:
             this.Image_cv = 'data:image/jpeg;base64,'+imageData;
             break;*/
          case 3:
            this.Image_ci_anv = 'data:image/jpeg;base64,' + imageData;
            break;
          case 4:
            this.Image_ci_rev = 'data:image/jpeg;base64,' + imageData;
            break;
          /*case 5:
            this.Image_ci_ant = 'data:image/jpeg;base64,'+imageData;
            break;*/
          default:

        }

        /*this.base64.encodeFile(imageUri).then((imageData: string) => {
          debugger;
        }, (err) => {
          console.log(err);
        });*/

      })
        .catch(error => console.warn(error))

    }
    else {
      /*
      if ( !this.platform.is('android') ) //el filechoser solo sirve con android, descomentar en caso que haga falta
        this.fileChooser.open()
         .then(
           uri => {
           this.filePath.resolveNativePath(uri)
           .then(filePath => {
             if ( id == 2 ) {
               this.Image_cv = filePath;
             }
             else {
               this.Image_ci_ant = filePath;
             }
           });
         });
      }*/

    }


  }

  check() {
    this.nav.setRoot(LoginPage, {show: true});
  }

  filesAdded(event, id) {
    let files: FileList;
    switch (id) {
      case 2:
        files = this.nativeInputBtn.nativeElement.files;
        this.Image_cv = files[0];
        this.fileTitle1 = files[0].name;

        break;
      case 5:
        files = this.nativeInputBtn2.nativeElement.files;
        this.Image_ci_ant = files[0];
        this.fileTitle2 = files[0].name;

        break;
    }
  }

  changeCountry() {
    this.selectCountry = true;
  }

  @HostListener('keydown', ['$event']) onInputChange(event) {
    if (event.srcElement.tagName !== "INPUT") {
      return;
    }
    var code = event.keyCode || event.which;
    if (code === 13 || code === 9) {
      if (this.signupSlider.getActiveIndex() != 2) {
        //  this.keyboard.close();
        event.preventDefault();
        this.next();
      }
    }
  }

  openUrl(url) {
    this.iab.create(url);
  }

  openFile(op) {
    if (op == 1) {
      this.nativeInputBtn.nativeElement.click()
    }
    else {
      this.nativeInputBtn2.nativeElement.click()
    }
  }


}
