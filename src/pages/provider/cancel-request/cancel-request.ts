import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

import { providerProvider } from '../../../providers/provider/provider';
import { ClientHomePage } from '../../client/client-home/client-home';

import { UtilServiceProvider } from '../../../providers/util-service/util-service';


@Component({
  selector: 'page-cancel-request',
  templateUrl: 'cancel-request.html',
})
export class clientCancelRequestPage {
  FormCancel: FormGroup;
  submitAttempt: boolean = false;
  qtybolean:boolean=false;
  selectedItem: any;
  itemsCarsbrands = [];
  itemscarmodels= [];
  private api;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private events: Events,
    private ProviderProvider: providerProvider,
    public util: UtilServiceProvider
) {
  util.getBaseUrl().subscribe((base_url) => { this.api = base_url; });
   this.selectedItem = navParams.get('item');

    this.FormCancel = formBuilder.group(
        {
          observations: ['',Validators.compose([Validators.required])],
      }

    );




  }

  cancel(){
    debugger
    // this.status = this.showStatus(status);

     this.ProviderProvider.cancelRequest(this.selectedItem._id,this.FormCancel.value.observations)

     .then(rsp => {
       if ( rsp.success ) {

       var ctx = this;
       setTimeout(function(){ctx.events.publish('hide:loading')},1500);
       this.redired();
      }
      else {
        this.events.publish('show:popup','Error al Cancelar solicitud, intente nuevamente');
      }
     },
     error => {
       this.events.publish('show:popup','Error al Cancelar solicitud, intente nuevamente');
     })
     .catch( (err) => {
       this.events.publish('show:popup','Error al Cancelar solicitud, intente nuevamente');
     })
   }

   redired() {
    this.navCtrl.push(ClientHomePage, {

    });
  }


}
