import {Component} from '@angular/core';
import {NgForm, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {NavController, Events, NavParams} from 'ionic-angular';
import {AuthServiceProvider} from '../../../providers/auth-service/auth-service';
import {RegisterPage} from '../../client/register/register';
import {HomePage} from '../../admin/home/home';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {RegisterWasherPage} from '../../provider/register-washer/register-washer';

import {ClientHomePage} from '../../client/client-home/client-home';
import {ProviderHomePage} from '../../provider/provider-home/provider-home';
import {AlertController} from 'ionic-angular';

@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  registerCredentials: FormGroup;
  submitAttempt: boolean = false;
  showLogin: boolean = false;

  constructor(
    private navCtrl: NavController,
    public formBuilder: FormBuilder,
    private auth: AuthServiceProvider,
    public events: Events,
    private fb: Facebook,
    public navParams: NavParams,
    public alertCtrl: AlertController
  ) {

    this.registerCredentials = formBuilder.group({
      email: ['', Validators.compose([Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'), Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });

    this.events.subscribe('show:login', () => {
      this.showLogin = true;
    });

    if (!!navParams.get("show")) {
      this.showLogin = navParams.get("show")
    }

  }

  public createAccount() {
    this.navCtrl.push(RegisterPage);
  }

  public createAccountWasher() {
    this.navCtrl.push(RegisterWasherPage);
  }

  public LoginPage(form: NgForm) {

    if (this.registerCredentials.valid) {
      this.submitAttempt = false;
      this.events.publish('show:loading');
      this.auth.login(this.registerCredentials.value)
        .then(rsp => {
            if (rsp.success) {
              switch (rsp.role) {
                case  'admin' :
                  this.events.publish('show:menu', rsp.role);
                  this.events.publish('hide:loading');
                  this.navCtrl.setRoot(HomePage);
                  break;
                case 'customer' :
                  this.events.publish('show:menu', rsp.role);
                  this.events.publish('hide:loading');
                  this.navCtrl.setRoot(ClientHomePage);
                  break;
                case 'provider' :
                  this.events.publish('show:menu', rsp.role);
                  this.events.publish('hide:loading');
                  this.navCtrl.setRoot(ProviderHomePage);
                  break;
                default :
                  this.events.publish('hide:loading');
                  this.events.publish('user:logout');
                  return;
              }
              this.events.publish('saveNotificationToken');
            }
            else {
              this.events.publish('show:error', 'Access Denied');
            }
          },
          error => {
            this.events.publish('show:error', !error.description ? 'Acceso negado' : error.description);
          })
        .catch((err) => {
          console.log(err);
          this.events.publish('show:error', 'Acceso negado');
        })

    }
    else {
      this.submitAttempt = true;

    }
  }

  public loginFacebook() {

    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.auth.facebookLogin(res.authResponse.accessToken)
          .then(rsp => {
              if (rsp.success) {
                switch (rsp.role) {
                  case  'admin' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.navCtrl.setRoot(HomePage);
                    break;
                  case 'customer' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.navCtrl.setRoot(ClientHomePage);
                    break;
                  case 'provider' :
                    this.events.publish('show:menu', rsp.role);
                    this.events.publish('hide:loading');
                    this.navCtrl.setRoot(ProviderHomePage);
                    break;
                  default :
                    this.events.publish('hide:loading');
                    this.events.publish('user:logout');
                    return;
                }
                this.events.publish('saveNotificationToken');

              }
              else {
                this.events.publish('show:error', 'Error logging into Facebook');
              }
            },
            error => {
              console.log(error);
              this.events.publish('show:error', 'Error logging into Facebook');
            })
          .catch((err) => {
            console.log(err);
            this.events.publish('show:error', 'Error logging into Facebook');

          })
      })
      .catch(e => console.log('Error en autenticación de Facebook', e));
  }

  forgot() {
    let prompt = this.alertCtrl.create({
      title: 'Recuperar Contraseña',
      message: "Ingrese su correo: ",
      inputs: [
        {
          name: 'Correo',
          placeholder: 'correo@ejemplo.com'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: data => {
            // debugger;
            this.auth.forgotPassword(data.Correo)
              .then(rsp => {
                  if (rsp.success) {
                    this.events.publish('show:popup', 'Éxito', 'Le fue enviada la contraseña al correo indicado.');
                  }
                  else {
                    this.events.publish('show:error', 'Se produjo un error, intente nuevamente!');
                  }
                },
                error => {
                  this.events.publish('show:error', !error.description ? 'Se produjo un error: ' : error.description);
                })
              .catch((err) => {
                this.events.publish('show:error', 'Se produjo un error, intente nuevamente!');
              })

          }
        }
      ]
    });
    prompt.present();
  }


}
