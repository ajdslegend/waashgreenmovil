import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {UtilServiceProvider} from "../../providers/util-service/util-service";
import {CouponProvider} from "../../providers/coupon/coupon";

import {OrderSummaryPage} from "../client/order-summary/order-summary";

/**
 * Generated class for the DiscountsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-discounts',
  templateUrl: 'discounts.html',
})
export class DiscountsPage {

  model: FormGroup;

  data = [];
  data1: any = [];

  coupons = [];

  isSearch = false;

  pgRequest: any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public util: UtilServiceProvider,
              private _coupon: CouponProvider,
              private events: Events,
              private formBuilder: FormBuilder) {
    this.modelForm();
    // this.getCoupon1();
    this.getCoupon();
    // this.getCoupon1();
    this.getCampaign();
    this.pgRequest = this.navParams.get('pgRequest');
    // this.scheduledTaskMinute(10);
    // this.scheduledTaskHours(19, 11, this.task())
    console.log("##pgRequest: " + JSON.stringify(this.pgRequest));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiscountsPage');
  }


  modelForm() {
    this.model = this.formBuilder.group({
      coupon_code: [null, Validators.required]
    });
  }

  reserveCoupon(form: NgForm) {
    let sendData = form;
    this.events.publish('show:loading');
    this._coupon.addCoupons(sendData).then((res) => {
      if (res.success) {
        console.log("##->res: " + JSON.stringify(res))
        // this.events.publish('hide:loading');
        // this.events.publish('show:popup', '', 'El cupon se agregó  éxitosamente.');
        // this.cooupon_code = sendData["coupon_code"];
        // this.getCoupon1();
        // this.getCoupon1();
        try {
          if (this.pgRequest.isNext) {
            console.log("##isNext: " + this.pgRequest.isNext);
            this.getCoupon1();
            this.events.publish('hide:loading');
            this.events.publish('show:popup', '', 'El cupon se agregó  éxitosamente.');
            this.gotoPage1(this.pgRequest.next, sendData["coupon_code"], res.data._id, this.pgRequest.dataNext);
            this.model.reset();
            // this.getCoupon();
            this.isSearch = false;
            //this.getCoupon1();
          }
        } catch (e) {
          console.log("##error: " + e.message)
          // console.log("##isNext: " + false)
          this.getCoupon1();
          this.events.publish('hide:loading');
          this.events.publish('show:popup', '', 'El cupón se agregó  éxitosamente.');
          this.model.reset();
          // this.getCoupon();
          this.isSearch = false;
        }

      }

      // else {
      //   this.events.publish('show:popup', 'El cupón ha sido agregado anteriormente');
      //   this.events.publish('hide:loading');
      // }

    }, (err0) => {
      this.events.publish('show:popup', 'El cupón ha sido agregado anteriormente');
      this.events.publish('hide:loading');
    }).catch((err1) => {
      this.model.reset();
      // this.getCoupon();
      this.isSearch = false;
      this.events.publish('show:popup', '!!Error al cargar cupon, intente nuevamente');
      this.events.publish('hide:loading');
    });
  }

  gotoPage(page: any, coupon: string, payload: any) {

    console.log("#page: " + page + " #coupon: " + coupon + " #payload: " + JSON.stringify(payload));

    let data = [];
    data = this.getFilterCoupon(coupon);
    console.log("##data: " + JSON.stringify(data));
    // let payload = {};
    payload["coupon_code"] = data[0].name_campaign;
    payload["discount"] = data[0].detail.discount;
    payload["isBack"] = true;
    if (page === "OrderSummaryPage") {
      console.log("##payload: " + JSON.stringify(payload));

      // this.navCtrl.popTo(OrderSummaryPage, {
      //   newRequest: payload
      // });

      // setTimeout(() => {
      // this.navCtrl.popTo(OrderSummaryPage, {
      //   newRequest: payload
      // });
      // this.navCtrl.getPrevious().data.request = payload;
      // this.navCtrl.getPrevious().

      this.navCtrl.push(OrderSummaryPage, {
        request: payload
      });

      // this.navCtrl.push(page, {
      //   request: payload
      // });


      // this.navCtrl.pop();
      // }, 100)

      // let nav = this._app.getRootNav();
      // nav.push(OrderSummaryPage, {newRequest: payload})
    }
  }

  gotoPage1(page: any, coupon: string, _id: string, payload: any) {

    console.log("#page: " + page + " #coupon: " + coupon + " #_id: " + _id + " #payload: " + JSON.stringify(payload));

    let data = [];
    data = this.getFilterCoupon(coupon);
    console.log("##data: " + JSON.stringify(data));
    // let payload = {};
    payload["id_coupon"] = _id;
    payload["coupon_code"] = data[0].name_campaign;
    payload["discount"] = data[0].detail.discount;
    payload["isBack"] = true;
    if (page === "OrderSummaryPage") {
      console.log("##payload: " + JSON.stringify(payload));

      // this.navCtrl.popTo(OrderSummaryPage, {
      //   newRequest: payload
      // });

      // setTimeout(() => {
      // this.navCtrl.popTo(OrderSummaryPage, {
      //   newRequest: payload
      // });
      // this.navCtrl.getPrevious().data.request = payload;
      // this.navCtrl.getPrevious().

      this.navCtrl.push(OrderSummaryPage, {
        request: payload
      });

      // this.navCtrl.push(page, {
      //   request: payload
      // });


      // this.navCtrl.pop();
      // }, 100)

      // let nav = this._app.getRootNav();
      // nav.push(OrderSummaryPage, {newRequest: payload})
    }
  }

  getFilterCoupon(coupon) {
    return this.coupons.filter((k) => {
      const el = JSON.stringify(k);
      return (el.toUpperCase().indexOf(coupon.toUpperCase()) > -1);
    });
    // return [];
  }

  onEnter(value) {
    console.log("####value: " + value);
    /* if (value.trim() !== '' && value.length !== 0) {
       this.data1 = this.data1.filter((k) => {
         const el = JSON.stringify(k);
         return (el.toLowerCase().indexOf(value.toLowerCase()) > -1);
       });
       this.isSearch = true;
     } else {
       this.isSearch = false;
       this.getCampaign();
     }*/
  }

  getCampaign() {
    // this.events.publish('show:loading');
    this._coupon.getCampaigns().then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // res.data.map((i) => {
        //   this.data.push(i);
        // });
        this.data1 = res.data;
        this.coupons = res.data;
        //this.data1 = [];
        //this.events.publish('hide:loading');
      } else {
        // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  translateStatus(status) {
    let op: any = "";
    switch (status) {
      case "available":
        op = "Disponible";
        break;
      case "applied":
        op = "Reservados";
        break;
      case "used":
        op = "Usado";
        break;
      case "defeated":
        op = "Vencido";
        break;
    }
    return op;
  }

  task() {
    console.log('###acá va la tarea ', new Date());
  }

  getCoupon() {
    this.events.publish('show:loading');
    this._coupon.getCoupons().then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // res.data.map((i) => {
        //   this.data.push(i);
        // });
        this.data = res.data;
        this.coupons = res.data;
        // this.data = [];
        this.events.publish('hide:loading');
      } else {
        this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  getCoupon1() {
    // this.events.publish('show:loading');
    this._coupon.getCoupons().then((res) => {
      if (res.success) {
        console.log("##res: " + JSON.stringify(res))
        // res.data.map((i) => {
        //   this.data.push(i);
        // });
        this.data = res.data;
        // this.coupons = res.data;
        // this.data = [];
        // this.events.publish('hide:loading');
      } else {
        // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
      }
    }, (err) => {
      // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    }).catch((err) => {
      // this.events.publish('show:popup', 'Error al cargar cupon, intente nuevamente');
    })
  }

  scheduledTaskHours(hora, minutos, tarea) {
    let ahora = new Date();
    console.log('#scheduledTaskHours->lanzado', ahora);
    var momento = new Date(ahora.getFullYear(), ahora.getMonth(), ahora.getDate(), hora, minutos);
    if (momento <= ahora) { // la hora era anterior a la hora actual, debo sumar un día
      momento = new Date(momento.getTime() + 1000 * 60 * 60 * 24);
    }
    console.log('#scheduledTaskHours->para ser ejecutado en', momento);
    setTimeout(() => {
      try {
        tarea();
        console.log('##setTimeout----> ');
      } catch (e) {

      }
      this.scheduledTaskHours(hora, minutos, tarea);
    }, momento.getTime() - ahora.getTime());
  }

  scheduledTaskMinute(minutos): void {
    let ahora = new Date();
    console.log('#scheduledTaskMinutes->lanzado', ahora);
    var momento = new Date(ahora.getFullYear(), ahora.getMonth(), ahora.getDate(), ahora.getHours(), minutos);
    console.log('#scheduledTaskMinutes->init: ', momento);
    console.log('#scheduledTaskMinutes->now: ', ahora);
    if (momento <= ahora) {
      momento = new Date(momento.getTime() + 1000 * 60);
      console.log('#scheduledTaskMinutes->process: ', momento);
    }
    console.log('#scheduledTaskMinutes->para ser ejecutado en', momento);
    console.log('##momento.getTime() - ahora.getTime():', momento.getTime() - ahora.getTime());
    setTimeout(() => {
      // tarea();
      console.log('##setTimeout---->');
      try {
        // tarea();
        this.task();
        // console.log('##setTimeout---->tarea: '+tarea);
      } catch (e) {

      }
      this.scheduledTaskMinute(minutos);
    }, momento.getTime() - ahora.getTime());
  }
}
